#%%Estimate Average Cluster Size, #MinPts Cluster, #Epsilon Threshold
#These parameters really depend on reference singal data and should be known by user 

import DataLoader 
import DimensionReduction as DR
import numpy as np
import matplotlib.pyplot as plt
reload(DR)
reload(DataLoader)
threshold = 10

Epsilon_Threshold_Ang = .985
Epsilon_Threshold_Amp = 1.50 #Adjust Size of Epsilon
MinPts = 100
Extrapolate_Data = False
Total_Points = 100
#Turn on the plotting of Angle_Cluster Recommended set to False unless debugging
Plot_Each_Angle_Cluster = True

TrainingFileNameAmplitude = 'Training_Data_Amp.csv'
TrainingFileNamePhase ='Training_Data_Ang.csv'
NewFileNameAmplitude = 'New_Data_Amp.csv'
NewFileNamePhase = 'New_Data_Ang.csv'

Training_Amp = DataLoader.LoadData(TrainingFileNameAmplitude)
period  = DataLoader.getPeriod(TrainingFileNameAmplitude)
names = DataLoader.getNames(TrainingFileNameAmplitude)

U, S, V = DR.SVD(Training_Amp)

print U.shape
print S.shape
print V.shape 


maxperiods  = DR.maxSVDPeriod(5,V,period)

np.savetxt("MaxPeriods.csv", maxperiods, delimiter=',')

Ds, ind = DR.diffSVD(S,threshold)


DR.plotStem(S[0:50],'Eigenvalue Number','Eigenvalue','SVD Eigenvalues',ind)
DR.plotSVD(0,ind,V,period)

