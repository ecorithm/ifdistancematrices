# -*- coding: utf-8 -*-
"""
Created on Thu May 26 16:36:43 2016

@author: bradlittooy
"""
from sklearn import decomposition
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt

def pcaReduction(data):
    pca = decomposition.PCA(n_components = 2)
    pca.fit(data)
    pca_data = pca.transform(data)
    return pca_data

def SVD(data):
    U, S, V = sp.linalg.svd(data,full_matrices=False)
    return U, S, V

def maxSVDPeriod(numberOfPeriods,V,period):
    maxPeriods = np.transpose(period[np.argsort(abs(V))][:,-5:][...,::-1])
    return maxPeriods

def plotSVD(in1,in2,V,period):
    for i in range(in1,in2):
        plt.figure()
        markerline, stemlines, baseline = plt.stem(period,V[i,:])
        plt.setp(markerline, 'markerfacecolor', 'b')
        plt.setp(baseline, 'color', 'r', 'linewidth', 1)
        yaxisName= 'Eigenvector # %d' %(i+1) 
        plt.ylabel(yaxisName)
        plt.xlabel('Period')
        plt.title('SVD ' + yaxisName )
        xmin = -1
        xmax = period[0]
        axes = plt.gca()
        axes.set_xlim([xmin,xmax])

def diffSVD(S,threshold):
    dS = np.diff(S,1)
    for i in range(0,len(dS)):
        if dS[0]/dS[i] > threshold:
            break
    ind = i 
    return dS, ind

def plotStem(data,*arg):
    if len(arg)>3:
        markerline, stemlines, baseline = plt.stem(data[:arg[3]],label= 'Principal Eigenvalues')
        plt.setp(markerline, 'color', 'r')
        plt.setp(stemlines, 'color', 'r')
        markerline2, stemlines2, baseline2 = plt.stem(range(arg[3],len(data)),data[arg[3]:],label = 'Secondary Eigenvalues')
        plt.setp(markerline2, 'color', 'b')
        plt.setp(stemlines2, 'color', 'b')
        plt.setp(baseline2, 'color', 'b')
        plt.legend()
    else:
        markerline, stemlines, baseline = plt.stem(data)
    #plt.setp(markerline, 'color', 'r')
    xmin = -1
    xmax = len(data) + 1
    if len(arg) > 0:
        plt.xlabel(arg[0])
    if len(arg) > 1:
        plt.ylabel(arg[1])
    if len(arg) > 2:
        plt.title(arg[2])
    axes = plt.gca()
    axes.set_xlim([xmin,xmax])
    
    


    