# -*- coding: utf-8 -*-
"""
Created on Wed May 25 11:59:04 2016

@author: bradlittooy
"""
#Load Data Module

import pandas as pd
import numpy as np

def LoadData(file_name): 
    df = pd.read_csv(file_name,index_col=0,low_memory=False)
    data = np.array(df)[3:,1:].astype(np.float)
    data_out = np.transpose(data)
    return  data_out 

def getNames(file_name):
    df = pd.read_csv(file_name,index_col=0,low_memory=False)
    names =  np.array(df.columns.values)[1:]
    return names

def getPeriod(file_name):
    df = pd.read_csv(file_name,index_col=0,low_memory=False)
    period = np.array(df)[3:,0].astype(np.float)
    return period

def PointMapping(names,data):
    Point_Map = dict(zip(names,data))  
    return Point_Map
    
    


    

    
    
    
   
     