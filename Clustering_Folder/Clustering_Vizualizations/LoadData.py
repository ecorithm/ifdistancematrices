# -*- coding: utf-8 -*-
"""
Created on Wed May 25 11:59:04 2016

@author: bradlittooy
"""
#Load Data Module

import pandas as pd
import numpy as np
from sklearn import decomposition

def LoadData(file_name): 
    df = pd.read_csv(file_name,index_col=0,low_memory=False)
    names_out =  np.array(df.columns.values)[1:]
    data = np.array(df)[3:,1:].astype(np.float)
    data_out = np.transpose(data)
    return names_out , data_out 

def PointMapping(names,data):
    Point_Map = dict(zip(names,data))  
    return Point_Map

def PlotData(data):
    pca = decomposition.PCA(n_components = 2)
    pca.fit(data)
    pca_data = pca.transform(data)
    return pca_data

def SVD(data):
    data_svd = np.linalg.svd(data)
    #add plotting
    return data_svd
    
    
   
     