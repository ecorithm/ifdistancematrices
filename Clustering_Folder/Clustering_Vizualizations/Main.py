# -*- coding: utf-8 -*-
"""
Created on Thu May 26 18:11:29 2016

@author: bradlittooy
"""
import DataLoader 
import DimensionReduction as DR
import numpy as np
import matplotlib.pyplot as plt
reload(DR)
reload(DataLoader)

Epsilon_Threshold_Ang = .985
Epsilon_Threshold_Amp = 1.50 #Adjust Size of Epsilon
MinPts = 100
Extrapolate_Data = False
Total_Points = 100
#Turn on the plotting of Angle_Cluster Recommended set to False unless debugging
Plot_Each_Angle_Cluster = True

#Specify the names of file
TrainingFileNameAmplitude = 'Training_Data_Amp.csv'
TrainingFileNamePhase ='Training_Data_Ang.csv'
NewFileNameAmplitude = 'New_Data_Amp.csv'
NewFileNamePhase = 'New_Data_Ang.csv'

Training_Amp = DataLoader.LoadData(TrainingFileNameAmplitude)
period  = DataLoader.getPeriod(TrainingFileNameAmplitude)
names = DataLoader.getNames(TrainingFileNameAmplitude)
