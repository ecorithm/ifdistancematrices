# Clustering Algorithms
Run_DBSCAN is used to run DBSCAN using both amplitude and angle spectral data. This uses diffusion_maps as well as csvreader2.m. An image of the flow function for this code is shown under DBSCANFlow.png.  All data is stored under DATA folder. Storage contains other clustering algorithms such as Kmeans which were used initially during research.
drtoolbox contains a number of dimension reduction codes and other useful clustering algorithms.
