%%Filmename_In_Amp/filenameIn_Ang is standard data, Filename_In_Comp is
%%added data. This Script takes the added data and compares it to the data
%%allready clustered, placing the new data in appropiate clusters..
function [] = Run_DBSCAN()
clc
clear
close all


%%Pick Data Day only 1 4 6,7,9,11 real data 10
%Real Data Dates 11/30/15 - 12/6/15 ESSB 275

%Choose baseline data set and comparison data set
Data_Set_Num = '9';
Data_Set_Num_Compare = '12';
Data_Set = ['Data/Set' Data_Set_Num '/' ];
Data_Set_Comp = ['Data/Set' Data_Set_Num_Compare '/' ];

%Algorithm Parameters
realdata = 0;
classify = 0;
start_comp = 12; %Start Value of Comparison Data
end_comp = 24; %End Value of Comparison Data
plot_ang_on = 0; %Turn clustering plotting of angles on Use when debugging epsilon for angle

%DBSCAN Parameters
MinPts = 4;
epsilon =.15;%.48 week ang amp % .09 for day %VAV_R35 Real Data 65 % VAV01 .20 % VAV01 Jan .23
epsilon_Ang = 70; %.28 Angle Scale 250, %69.9 for no scale %set7 .273 %VAV_R35 .25 % VAV01 .28 Dec % VAV01 .28 Jan



filenameIn_Amp_day = [Data_Set 'data_test_spectral_Amp_day_' Data_Set_Num '.csv'];
filenameIn_Amp_week = [Data_Set 'data_test_spectral_Amp_week_' Data_Set_Num '.csv'];
filenameIn_Ang_day = [Data_Set 'data_test_spectral_Ang_day_' Data_Set_Num '.csv'];
filenameIn_Ang_week = [Data_Set 'data_test_spectral_Ang_week_' Data_Set_Num '.csv'];

if realdata
    filenameIn_Ang_day_Comp = [Data_Set_Comp 'VAV_R01_Ang_Jan.csv'];
    filenameIn_Amp_day_Comp = [Data_Set_Comp 'VAV_R01_Amp_Jan.csv'];
else
    filenameIn_Amp_day_Comp = [Data_Set_Comp 'data_test_spectral_Amp_day_' Data_Set_Num_Compare '.csv'];
    filenameIn_Amp_week_Comp = [Data_Set_Comp 'data_test_spectral_Amp_week_' Data_Set_Num_Compare '.csv'];
    filenameIn_Ang_day_Comp = [Data_Set_Comp 'data_test_spectral_Ang_day_' Data_Set_Num_Compare '.csv'];
    filenameIn_Ang_week_Comp = [Data_Set_Comp 'data_test_spectral_Ang_week_' Data_Set_Num_Compare '.csv'];
end


Go_DBSCAN(classify,plot_ang_on,epsilon,epsilon_Ang,MinPts,filenameIn_Ang_day,filenameIn_Amp_day,...
    filenameIn_Amp_day_Comp,filenameIn_Ang_day_Comp,start_comp,end_comp)

    function []  = Go_DBSCAN(classify,plot_ang_on,epsilon,epsilon_Ang,MinPts,filenameIn_Ang_day,filenameIn_Amp_day,...
            filenameIn_Amp_day_Comp,filenameIn_Ang_day_Comp,start_comp,end_comp)
        
        %Choose type of data
        Amp_day = 1;
        Amp_week = 0;
        Ang_day = 1;
        Ang_week =0;
        
        Use_Amp = 0; %Use Amplitude
        Use_Ang = 0; %Use Angle
        Use_Amp_Angle = 0; %Use Amplitude and Angle
        Use_Cos_Ang = 0; %Use Cosine of Angle
        Amp_Ang_sep = 1; %Cluster ampltiude then cluster angle
        
        scale_angle = 0; %Scale the angle to magnitude of amplitude, only affects plotting
        scale = 100;
        
        %Estimate eplison tool
        Est_Epsilon = 0;
        hist_on = 1;
        pcolor_on = 0;
        
        %Dimension Reduction Parameters
        Use_dim_red_DB=0;
        Use_dim_red_DB_ang = 0;
        dimension_reduction = 'diffusion_maps'; %diffusion_maps %pca
        dim_red = str2func(dimension_reduction);
        dimplot=2;
        alpha = 1; %Diffusion_Maps Parameters
        sigma = 10;
        
        %Distance Metric
        d_type_Amp = 'minkowski';%'euclidean' 'seuclidean' 'mahalanobis' 'minkowski' 'correlation' 'cosine' 'cityblock' 'Jaccard'
        d_type_Ang = 'minkowski';%'euclidean' 'seuclidean' 'mahalanobis' 'minkowski' 'correlation' 'cosine' 'cityblock' 'Jaccard'
        
        %Load Data (Standard Vs. Comparison)
        [data_Amp,data_Ang,names,data_plot] = DataLoader(filenameIn_Ang_day,filenameIn_Amp_day,scale,scale_angle);
        [data_Amp_comp,data_Ang_comp,names_comp,data_plot_comp] = DataLoader(filenameIn_Ang_day_Comp,filenameIn_Amp_day_Comp,scale,scale_angle);
        
        if end_comp > size(data_Amp_comp,1)
            disp('End point picked is greater than size of data, Setting End point to last point in data set')
            end_comp = size(data_Amp_comp,1);
        end
        function [data_Amp,data_Ang,names,data_plot] = DataLoader(filenameIn_Ang_day,filenameIn_Amp_day,scale,scale_angle)
            
            %%Load Data
            disp('Loading Data...')
            tic
            if Amp_day
                data_Amp=csvread(filenameIn_Amp_day,4,2)';
                [~, names, ~] = csvreader2(filenameIn_Amp_day);
                names = names(3:end)';
            end
            if Amp_week
                data_Amp = csvread(filenameIn_Amp_week,4,2)';
                [~, names, ~] = csvreader2(filenameIn_Amp_week);
                names = names(3:end)';
            end
            if Ang_day
                data_Ang = csvread(filenameIn_Ang_day,4,2)';
                [~, names, ~] = csvreader2(filenameIn_Ang_day);
                names = names(3:end)';
            end
            if Ang_week
                data_Ang = csvread(filenameIn_Ang_week,4,2)';
                [~, names, ~] = csvreader2(filenameIn_Ang_week);
                names = names(3:end)';
            end
            
            if scale_angle
                data_Ang = data_Ang/scale;
            end
            
            data_plot =[data_Amp(:,1:end);data_Ang(:,1:end)];
            
            toc
            
        end
        
        
        if Est_Epsilon
            disp('Performing Epsilon Estimate...')
            est_epsilon(data_Amp,MinPts,d_type_Amp,hist_on,pcolor_on);
            toc
        end
        
        function [epsil,IDX,D,SD] = est_epsilon(X,MinPts,d_type,hist_on,pcolor_on)
            n=size(X,1);
            count =0;
            D=pdist2(X,X,d_type);
            for ind1=1:n
                [IDX,D]=knnsearch(X(n,:),X);
                SD = SAM(X(n,:),X);
                %D
                %D(all(D==0,2),:)=[];
                %SD(all(D==0,2),:)=[];
                length(D);
                size(D);
                stor(n,:) = SD;
                epsil(n,:) = D;
                %count = count + 1
            end
            if hist_on
                
                hist(epsil)
                figure
            end
            if pcolor_on
                pcolor(epsil);
                shading flat
                figure
            end
        end
        
        %DBSCAN Algorithm
        
        %Note DBSCAN runs twice, idx_DB is clusters for amplitide only,
        %idx_DB_new include anlge in calculations..
        
        disp('Performing DBSCAN...')
        
        %First Cluster baseline data set
        
        idx_base = DBSCAN(data_Amp,epsilon,MinPts,d_type_Amp); %Amplitude Cluster
        idx_base_new = clust_ang(idx_base,data_Ang,epsilon_Ang,MinPts,d_type_Ang,dimplot,alpha,sigma,plot_ang_on);
        
        
        cluster_class(idx_base_new,data_Amp_comp,data_Ang_comp,data_Amp,data_Ang,dimension_reduction);
        
        %%Plot Classify Clusters
        
        class_plot= diffusion_maps(data_plot_new,dimplot,alpha,sigma);
        PlotClusterinResult(diffusion_maps(data_plot,dimplot,alpha,sigma),idx_base_new,'Amplitude and Angle Class',0)
        figure
        PlotClusterinResult(class_plot, idx_class_ang ,'Classification Plot',1);
        
        
        
        function [IDX, isnoise]=DBSCAN(X,epsilon,MinPts,d_type,~)
            
            C=0;
            
            n=size(X,1);
            IDX=zeros(n,1);
            if strcmpi(d_type,'minkowski')
                D=pdist2(X,X,d_type);
            else
                D=pdist2(X,X,d_type);
            end
            
            visited=false(n,1);
            isnoise=false(n,1);
            
            for i=1:n
                if ~visited(i)
                    visited(i)=true;
                    
                    Neighbors=RegionQuery(i);
                    if numel(Neighbors)<MinPts
                        % X(i,:) is NOISE
                        isnoise(i)=true;
                    else
                        C=C+1;
                        ExpandCluster(i,Neighbors,C);
                    end
                    
                end
                
            end
            
            function ExpandCluster(i,Neighbors,C)
                IDX(i)=C;
                
                k = 1;
                while true
                    j = Neighbors(k);
                    
                    if ~visited(j)
                        visited(j)=true;
                        Neighbors2=RegionQuery(j);
                        if numel(Neighbors2)>=MinPts
                            Neighbors=[Neighbors Neighbors2];   %#ok
                        end
                    end
                    if IDX(j)==0
                        IDX(j)=C;
                    end
                    
                    k = k + 1;
                    if k > numel(Neighbors)
                        break;
                    end
                end
            end
            
            function Neighbors=RegionQuery(i)
                Neighbors=find(D(i,:)<=epsilon);
            end
            
        end
        function [idx_DB_new] = clust_ang(idx_DB,data_Ang,epsilon_Ang,MinPts,d_type_Ang,dim,alpha,sigma,plot_ang_on)
            if max(idx_DB) == 0
                error('Adjust Amplitude Epsislon value, All data is noise')
            end
            for f = 0:max(idx_DB); %%%%NOT SURE IF SHOULD START AT 0 or 1!!!
                int=  find(idx_DB == f);
                Data_Ang_new = data_Ang(int,:);
                if f ==0
                    idx_DB_new(int) = 0;
                else
                    idx_DB_ang = DBSCAN(Data_Ang_new,epsilon_Ang,MinPts,d_type_Ang);
                    if plot_ang_on
                        figure
                        PlotClusterinResult(diffusion_maps(Data_Ang_new,dim,alpha,sigma),idx_DB_ang,['Angle Cluster' num2str(f)],0)
                        
                        
                    end
                    if max(idx_DB_ang) <= 1
                        
                        idx_DB_new(int) = f;
                        
                    else max(idx_DB_ang) > 1; %If more than one cluster found in amplitude clusters
                        int1 = int(find(idx_DB_ang<=1));
                        int2 = int(find(idx_DB_ang>1));
                        idx_DB_new(int1) = idx_DB(int1);
                        idx_DB_new(int2) =  idx_DB_ang(find(idx_DB_ang>1)) +max(idx_DB)-1 ;
                    end
                end
            end
        end
        function [] = ...
                cluster_class(idx_base_new,data_Amp_comp,data_Ang_comp,data_Amp,data_Ang,dimension_reduction)
            num = 1;
            start_class = length(idx_base_new)+1;
            
            %Class Amp
            plot_ang_on = 0;
            
            if size(data_Ang_comp,2)~=size(data_Ang,2); %%%% Problem Area
                if size(data_Ang_comp,2) > size(data_Ang,2)
                    error('Comparison Data is Larger than Baseline data, Recomend using more baseline data')
                end
                disp('Data Set Sizes are Not equal, reducing dimension')
                dim = size(data_Ang_comp,2);
                if strcmpi('diffusion_maps',dimension_reduction);
                    data_Ang= diffusion_maps(data_Ang,dim,alpha,sigma);
                    data_Amp= diffusion_maps(data_Amp,dim,alpha,sigma);
                    %data_Ang_comp = diffusion_maps(data_Ang_comp',dim,alpha,sigma);
                    %data_Amp_comp = diffusion_maps(data_Amp_comp',dim,alpha,sigma);
                else strcmpi('pca',dimension_reduction);
                    data_Ang_= pca(data_Ang',dim);
                    data_Amp= pca(data_Amp',dim);
                    %data_Ang_comp= pca(data_Ang_comp',dim);
                    %data_Amp_comp= pca(data_Amp_comp',dim);
                end
                data_class = [data_Amp ;data_Amp_comp(start_comp:end_comp,:)];
                %data_class = diffusion_maps(data_class,dim,alpha,sigma);
            else
                data_class = [data_Amp ;data_Amp_comp(start_comp:end_comp ,:)]; %Transpose Data so it conforms with DBSCAN!
            end
            %test_plot= diffusion_maps(data_class',2,alpha,sigma)
            %plot(test_plot(:,1),test_plot(:,1),'*')
            %%%%DEBUGGINGG
            idx_class_int = DBSCAN(data_class,epsilon,MinPts,d_type_Amp);
            if max(idx_class_int) == 0
                error('Episilon Value is too small, All points viewed as noise')
            end
            
            %Classify by angle
            data_class_ang = [data_Ang ;data_Ang_comp(start_comp:end_comp,:)];%Transpose Data so it conforms with DBSCAN!
            idx_class_ang = clust_ang(idx_class_int,data_class_ang,epsilon_Ang,MinPts,d_type_Ang,dimplot,alpha,sigma,plot_ang_on);
            
            
            idx_class = idx_class_ang(start_class:end);
            
            data_plot_new =[data_class(:,1:end);data_class_ang(:,1:end)];
            
            %Recording Faults
            for k = start_comp:end_comp
                
                
                if idx_class(num)==0
                    name_class{num} = names_comp{k};
                    fault_type{num} = 'Noise';
                else
                    name_class{num} = names_comp{k};
                    if idx_class(num) > max(idx_base_new)
                        fault_type{num} = ['New Fault' '  ' 'cluster' num2str(idx_DB_class(num))];
                    else
                        fault_type{num} = names{find(idx_class(num) == idx_base_new,1)};
                    end
                end
                num = num+1;
            end
            
            
        end
        function PlotClusterinResult(X, IDX,name,class_plot)
            
            z=max(IDX);
            
            Colors=hsv(z);
            
            Legends = {};
            if class_plot
                for q = 1:length(unique(IDX))
                    ind = unique(IDX);
                    ind = ind(q);
                    Xi=X(IDX==ind,:);
                    if ind~=0
                        Style = 's';
                        MarkerSize = 8;
                        Color = Colors(ind,:);
                        Legends{end+1} = ['Cluster Class #' num2str(q)];
                    else
                        Style = 'o';
                        MarkerSize = 6;
                        Color = [0 0 0];
                        if ~isempty(Xi)
                            Legends{end+1} = 'Noise';
                        end
                    end
                    if ~isempty(Xi)
                        plot(Xi(:,1),Xi(:,2),Style,'MarkerSize',MarkerSize,'Color',Color);
                        title(name)
                    end
                    hold on;
                end
            else
                for q=0:z
                    Xi=X(IDX==q,:);
                    if q~=0
                        Style = 'x';
                        MarkerSize = 8;
                        Color = Colors(q,:);
                        Legends{end+1} = ['Cluster #' num2str(q)];
                    else
                        Style = 'o';
                        MarkerSize = 6;
                        Color = [0 0 0];
                        if ~isempty(Xi)
                            Legends{end+1} = 'Noise';
                        end
                    end
                    if ~isempty(Xi)
                        plot(Xi(:,1),Xi(:,2),Style,'MarkerSize',MarkerSize,'Color',Color);
                        title(name)
                    end
                    hold on;
                end
            end
            %plot(X(end,1),X(end,2),'MarkerSize',MarkerSize,'color','y')
            hold off;
            axis equal;
            grid off;
            
            legend(Legends);
            
            
            
            
        end
        
        
    end

%%
%write File
disp('Writing Cluster Results To .txt File...')
tic

fid = fopen('Clusters.csv', 'w+');
for m = 1:length(idx_base_new)
    fprintf(fid, '%6.0f %10s\n',idx_base_new(m), names{m});
end


fileID = fopen('Clusters_Class.csv', 'w+');
formatSpec = '%s %d %2.1f %s\n';


nrows = length(idx_class);
formatspec = 'Cluster#: %2.0f PointName:    %35s     FaultType:     %12s\n';
for i = 1:nrows
    
    fprintf(fileID,formatspec,idx_class(i),name_class{i},fault_type{i});
    
end
fclose(fileID);
toc
title(['DBSCAN Clustering (\epsilon = ' num2str(epsilon) ', MinPts = ' num2str(MinPts) ')']);



disp('Finished')
end
