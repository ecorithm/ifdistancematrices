%
% Copyright (c) 2015, Yarpiz (www.yarpiz.com)
% All rights reserved. Please read the "license.txt" for license terms.
%
% Project Code: YPML110
% Project Title: Implementation of DBSCAN Clustering in MATLAB
% Publisher: Yarpiz (www.yarpiz.com)
% 
% Developer: S. Mostapha Kalami Heris (Member of Yarpiz Team)
% 
% Contact Info: sm.kalami@gmail.com, info@yarpiz.com
%class_plot is 0 or 1. Set to 1 for classifying plots

function PlotClusterinResult(X, IDX,name,class_plot)

k=max(IDX);

Colors=hsv(k);

Legends = {};
if class_plot
    for i = 1:length(unique(IDX))
        ind = unique(IDX);
        ind = ind(i);
        Xi=X(IDX==ind,:);
        if ind~=0
            Style = 's';
            MarkerSize = 8;
            Color = Colors(ind,:);
            Legends{end+1} = ['Cluster #' num2str(i)];
        else
            Style = 'o';
            MarkerSize = 6;
            Color = [0 0 0];
            if ~isempty(Xi)
                Legends{end+1} = 'Noise';
            end
        end
        if ~isempty(Xi)
            plot(Xi(:,1),Xi(:,2),Style,'MarkerSize',MarkerSize,'Color',Color);
        end
        hold on;
    end
else
    for i=0:k
        Xi=X(IDX==i,:);
        if i~=0
            Style = 'x';
            MarkerSize = 8;
            Color = Colors(i,:);
            Legends{end+1} = ['Cluster #' num2str(i)];
        else
            Style = 'o';
            MarkerSize = 6;
            Color = [0 0 0];
            if ~isempty(Xi)
                Legends{end+1} = 'Noise';
            end
        end
        if ~isempty(Xi)
            plot(Xi(:,1),Xi(:,2),Style,'MarkerSize',MarkerSize,'Color',Color);
            title(name)
        end
        hold on;
    end
end
plot(X(end,1),X(end,2),'MarkerSize',MarkerSize,'color','y')
hold off;
axis equal;
grid off;
%if ~ class_plot 
legend(Legends);
%

%end