clc
clear
close all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Pick Data Day only 1 4 6,7,9,11 real data 10
%Real Data Dates 11/30/15 - 12/6/15 ESSB 275
Data_Set_Num = '12';
Data_Set_Num_Compare = '1';
Data_Set = ['Data/Set' Data_Set_Num '/' ];
Data_Set_Comp = ['Data/Set' Data_Set_Num_Compare '/' ];

%Algorithm Parameters
realdata = 0;
classify = 1;
start_comp = 1;
end_comp = 1000;
plot_ang_on = 0; %Turn clustering plotting of angles on Use when debugging epsilon for angle
MinPts = 20;
epsilon =.1;%.48 week ang amp % .09 for day %VAV_R35 Real Data 65 % VAV01 .20 % VAV01 Jan .23
epsilon_Ang = 68; %.28 Angle Scale 250, %69.9 for no scale %set7 .273 %VAV_R35 .25 % VAV01 .28 Dec % VAV01 .28 Jan



    filenameIn_Amp_day = [Data_Set 'data_test_spectral_Amp_day_' Data_Set_Num '.csv'];
    filenameIn_Amp_week = [Data_Set 'data_test_spectral_Amp_week_' Data_Set_Num '.csv'];
    filenameIn_Ang_day = [Data_Set 'data_test_spectral_Ang_day_' Data_Set_Num '.csv'];
    filenameIn_Ang_week = [Data_Set 'data_test_spectral_Ang_week_' Data_Set_Num '.csv'];

    if realdata
        filenameIn_Ang_day_Comp = [Data_Set_Comp 'VAV_R01_Ang_Jan.csv'];
        filenameIn_Amp_day_Comp = [Data_Set_Comp 'VAV_R01_Amp_Jan.csv'];
    else
        filenameIn_Amp_day_Comp = [Data_Set_Comp 'data_test_spectral_Amp_day_' Data_Set_Num_Compare '.csv'];
        filenameIn_Amp_week_Comp = [Data_Set_Comp 'data_test_spectral_Amp_week_' Data_Set_Num_Compare '.csv'];
        filenameIn_Ang_day_Comp = [Data_Set_Comp 'data_test_spectral_Ang_day_' Data_Set_Num_Compare '.csv'];
        filenameIn_Ang_week_Comp = [Data_Set_Comp 'data_test_spectral_Ang_week_' Data_Set_Num_Compare '.csv'];
    end
    
    
Go_DBSCAN(classify,plot_ang_on,epsilon,epsilon_Ang,MinPts,filenameIn_Ang_day,filenameIn_Amp_day,...
    filenameIn_Amp_day_Comp,filenameIn_Ang_day_Comp,start_comp,end_comp)



















