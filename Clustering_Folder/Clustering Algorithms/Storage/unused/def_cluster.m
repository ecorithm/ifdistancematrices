function [] = def_cluster(filenameIn_Ang_day,filenameIn_Amp_day,Amp_day,Amp_week,Ang_week,Ang_day,Use_Amp,Use_Ang,Use_Amp_Angle,Use_Cos_Ang,Amp_Ang_sep,scale_angle,scale,...
    Est_Epsilon,hist_on,pcolor_on)




%%%Chose Algorithms
%%
%%DBSCAN

if Amp_day;
epsilon =.09;%.48 week ang amp % .09 for day %VAV_R35 Real Data 65 % VAV01 .20 % VAV01 Jan .23
epsilon_Ang = .273; %.28 Angle Scale 250, %69.9 for no scale %set7 .273 %VAV_R35 .25 % VAV01 .28 Dec % VAV01 .28 Jan
else Amp_week;
    epsilon =.09;
    epsilon_Ang = .28;
end
MinPts = 3;
Use_dim_red_DB=0;
Use_dim_red_DB_ang = 0;
d_type_Amp = 'minkowski';%'euclidean' 'seuclidean' 'mahalanobis' 'minkowski' 'correlation' 'cosine' 'cityblock' 'Jaccard'
d_type_Ang = 'minkowski';%'euclidean' 'seuclidean' 'mahalanobis' 'minkowski' 'correlation' 'cosine' 'cityblock' 'Jaccard'
dimension_reduction = 'diffusion_maps'; %diffusion_maps %pca
dim_red = str2func(dimension_reduction);
dim = 2;
alpha = 1;
sigma = 10;
plot_ang_on = 0;
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%Load Data 
disp('Loading Data...')
tic
if Amp_day
    data_Amp=csvread(filenameIn_Amp_day,4,2);
    [~, names, ~] = csvreader2(filenameIn_Amp_day);
    names = names(3:end)';
end
if Amp_week
    data_Amp = csvread(filenameIn_Amp_week,4,2);
    [~, names, ~] = csvreader2(filenameIn_Amp_week);
    names = names(3:end)';
end
if Ang_day
    data_Ang = csvread(filenameIn_Ang_day,4,2);
    [~, names, ~] = csvreader2(filenameIn_Ang_day);
    names = names(3:end)';
end
if Ang_week
    data_Ang = csvread(filenameIn_Ang_week,4,2);
    [~, names, ~] = csvreader2(filenameIn_Ang_week);
    names = names(3:end)';
end
toc


%%% Pick Data
%%
if scale_angle
    data_Ang = data_Ang/scale;
end

if Use_Amp
    data = data_Amp;
end

if Use_Ang
    if Use_Cos_Ang
        data = cos(data_Ang);
    else
        data = data_Ang;
    end
end

if Use_Amp_Angle
    if Use_Cos_Ang
        data_Ang = cos(data_Ang);
    end
    data = [data_Amp;data_Ang];
end

if Amp_Ang_sep
    data = data_Amp(:,1:end);
    data_plot =[data_Amp(:,1:end);data_Ang(:,1:end)];
end

disp('Performing Epsilon Estimate...')
tic
if Est_Epsilon
est_epsilon(data',MinPts,d_type_Amp,hist_on,pcolor_on);
%[SD] = est_epsilon(data_Ang',MinPts,d_type_Ang,hist_on,pcolor_on);
toc
end
%break

%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DBSCAN

    disp('Perfoming DBSCAN...')
    tic
    if Use_dim_red_DB;
        if strcmpi('pca',dimension_reduction)
            data_DB = pca(data');
            data_Ang_DB = pca(data_Ang');
            data_Amp_DB = pca(data_Amp');
        else strcmpi('diffusion_maps',dimension_reduction)
            data_DB = diffusion_maps(data',dim,alpha,sigma);
            data_Ang_DB = diffusion_maps(data_Ang',dim,alpha,sigma);
            data_Amp_DB = diffusion_maps(data_Amp',dim,alpha,sigma);
        end
     if Use_dim_red_DB_ang
         if strcmpi('pca',dimension_reduction)
            data_Ang_DB = pca(data_Ang');
        else strcmpi('diffusion_maps',dimension_reduction)
            data_Ang_DB = diffusion_maps(data_Ang',dim,alpha,sigma);
         end
     end
        
    else
        data_DB = data';
        data_Amp_DB = data_Amp';
        data_Ang_DB = data_Ang' ;
    end
    idx_DB = DBSCAN(data_DB,epsilon,MinPts,d_type_Amp);
    PlotClusterinResult(diffusion_maps(data_DB,dim,alpha,sigma),idx_DB,'Amplitude')
    figure
  
    if Amp_Ang_sep
        if max(idx_DB) == 0
            error('Adjust Epsislon value, All data is noise')
        end
        for i = 1:max(idx_DB);
            int=  find(idx_DB == i);
            Data_Ang_new = data_Ang_DB(int,:);
            idx_DB_ang = DBSCAN(Data_Ang_new,epsilon_Ang,MinPts,d_type_Ang);
            if plot_ang_on
                
         PlotClusterinResult(diffusion_maps(Data_Ang_new,dim,alpha,sigma),idx_DB_ang,['Angle Cluster' num2str(i)])
            
            figure
            title(num2str(i))
            end
            if max(idx_DB_ang) <= 1
               
                idx_DB_new(int) = i;
                    
            else max(idx_DB_ang) > 1;
                int1 = int(find(idx_DB_ang<=1));
                int2 = int(find(idx_DB_ang>1));
                idx_DB_new(int1) = idx_DB(int1);
                idx_DB_new(int2) =  idx_DB_ang(find(idx_DB_ang>1)) +max(idx_DB)-1 ;
            end
        end
    end
    for j = 1:length(idx_DB_new)
        int = find(idx_DB_new == j);
        Data_Amp_DB_Comp = data_DB(int,:)
        size(Data_Amp_DB_Comp)
        Avg = sum(Data_Amp_DB_Comp);
    end
        
    
  %%
  %plot results
    if strcmpi('pca',dimension_reduction);
        PlotClusterinResult(pca((data_plot)), idx_DB_new,'Combined Plot');
    else strcmpi('diffusion_maps',dimension_reduction);
        PlotClusterinResult(diffusion_maps(data_plot',dim,alpha,sigma),idx_DB_new,'Combined Plot')
    end
    toc
    %%
    %write File
    idx_DB;
    disp('Writing Cluster Results To .txt File...')
    tic
    
    fid = fopen('def_cluster.csv', 'w+');
    for j = 1:length(idx_DB)
        fprintf(fid, '%6.0f %10s\n',idx_DB_new(j), names{j});
        
    end
    toc
    title(['DBSCAN Clustering (\epsilon = ' num2str(epsilon) ', MinPts = ' num2str(MinPts) ')']);
    
