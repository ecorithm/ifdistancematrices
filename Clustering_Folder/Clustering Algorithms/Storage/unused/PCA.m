function [P,vp,V,D] = PCA(data,dim,C)
%                   %beletehet� hogy veszi e bementnek az eigenvektoroat, if existes t�ma
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% the data
[N,n]=size(data);
odim = dim; %projection to q-dimension
% calculating autocorrelation matrix
  A = zeros(n);
  me = zeros(1,n);
  for i=1:n, 
    me(i) = mean(data(isfinite(data(:,i)),i)); 
    data(:,i) = data(:,i) - me(i); 
  end  
  for i=1:n, 
    for j=i:n, 
      c = data(:,i).*data(:,j); c = c(isfinite(c));
      A(i,j) = sum(c)/length(c); A(j,i) = A(i,j); 
    end
  end
  
  % eigenvectors, sort them according to eigenvalues, and normalize
  [V,S]   = eig(A);
  eigval  = diag(S);
  [y,ind] = sort(abs(eigval)); 
  eigval  = eigval(flipud(ind));%??????
  V       = V(:,flipud(ind)); 
  for i=1:odim
     V(:,i) = (V(:,i) / norm(V(:,i)));
  end
  
  % take only odim first eigenvectors
  V = V(:,1:odim);
  D = abs(eigval)/sum(abs(eigval));
  D = D(1:odim); 

% project the data using odim first eigenvectors
P = data*V;
%cluster centers by PCA
vp = (C-me(ones(dim,1),:))*V; 

%results
P = P;%projected data
vp = vp;%cluster centers
V = V;%eigenvectors
D = D;%eigenvalues



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
