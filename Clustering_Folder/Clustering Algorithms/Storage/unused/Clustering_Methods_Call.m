clc
clear
close all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Pick Data

Amp_day = 0;
Amp_week = 1;
Ang_day = 0;
Ang_week =1;

Use_Amp = 0; %Use Amplitude 
Use_Ang = 0; %Use Angle 
Use_Amp_Angle = 1; %Use Amplitude and Angle
Use_Cos_Ang = 0; %Use Cosine of Angle
scale_angle = 1;
scale = 350;
%%%Chose Algorithms


%%DBSCAN
dbscan = 1;
epsilon =.415;
MinPts = 9;
Use_dim_red_DB=0;
d_type = 'minkowski'; 
dimension_reduction = 'pca';
dim_red = str2func(dimension_reduction);


%Diffusion Maps
diffusionMaps = 0;
alpha = 5; %parameters
sigmaK = 100;
d = 2; %dimension reduction


%Diffusion Maps w/DBSCAN
DB_Diffuse = 0;
epsilon_DM = .05;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Load Data
disp('Loading Data')
tic
if Amp_day
    data_Amp=csvread('data_test_spectral_Amp_day_6.csv',4,2);
end
if Amp_week
    data_Amp = csvread('data_test_spectral_Amp_week_6.csv',4,2);
end
if Ang_day
    data_Ang = csvread('data_test_spectral_Ang_day_6.csv',4,2);
end
if Ang_week
    data_Ang = csvread('data_test_spectral_Ang_week_6.csv',4,2);
end
toc

%%% Pick Data
if scale_angle 
    data_Ang = data_Ang/scale;
end


if Use_Amp
    data = data_Amp;
end

if Use_Ang
    if Use_Cos_Ang
        data = cos(data_Ang);
    else
        data = data_Ang;
    end
end

if Use_Amp_Angle
    if Use_Cos_Ang
        data_Ang = cos(data_Ang);
    end
    data = [data_Amp;data_Ang];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DBSCAN
if dbscan
    disp('Timing DBSCAN')
    tic
    if Use_dim_red_DB
        data_DB = dim_red(data');
        data_Ang_DB = dim_red(data_Ang');
        data_Amp_DB = dim_red(data_Amp');
    else
        data_DB = data';
        data_Amp_DB = data_Amp';
        data_Ang_DB = data_Ang' ;
    end
        idx_DB = DBSCAN(data_DB,epsilon,MinPts,d_type);
        if Use_dim_red_DB
            PlotClusterinResult((data_DB), idx_DB);
           
        else
            PlotClusterinResult(dim_red(data_DB), idx_DB);
        end 

    
    idx_DB
  
    title(['DBSCAN Clustering (\epsilon = ' num2str(epsilon) ', MinPts = ' num2str(MinPts) ')']);
    toc
end




%%kmeans
K_means = 1;
k =12; %Number of Clusters
r = 10; %number of initial centroid guesses
Use_dim_red_kmean=0;

%%kmediods
K_mediod = 0;
k_med =8; %Number of Clusters
r_med = 10; %number of initial centroid guesses
Use_dim_red_Kmed=1;


%Fuzzy_C_means
fuzzy_c = 0;
Nc = 8;
options = [2 NaN NaN 0];
Use_dim_red_fuz=0;

%Optics
Optics = 0;
Epsilon = .1;
minpts = 10;
Use_dim_red_op=0;

%Dimension Reduction Algorithm Selection
dim_reduction =0;
dimension_reduction = 'pca';

dim_red = str2func(dimension_reduction);

%Diffusion Maps
diffusionMaps = 0;
alpha = 5; %parameters
sigmaK = 100;
d = 2; %dimension reduction

%Diffusion Maps w/Kmeans
Diffus_Kmean = 0;

%Diffusion Maps w/DBSCAN
DB_Diffuse = 0;
epsilon_DM = .05;

%Isomap
run_isomap =0;
dim = 2;
neighboors = 9;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Kmeans
if K_means
    disp('Timing Kmeans')
    tic
    
    if Use_dim_red_kmean
        [data_k, score] = dim_red((data'),2);
    else
        data_k = data';
    end
    
    [idx_k,C,sumd,D_k] = kmeans((data_k),k,'distance','sqeuclidean','Replicates',r);
    
    idx_k
    toc
    
            title_diffuse = 0;
            if Use_dim_red_Kmed
                PlotClusterinResult((data_k), idx_k);

            else
                PlotClusterinResult(dim_red(data_k), idx_k);
            end 
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Kmediod
if K_mediod
    disp('Timing Kmediod')
    tic
    
    if Use_dim_red_Kmed
        [data_kmed, score] = pca((data),2);
    else
        data_kmed = data;
    end
    
    [idx_kmed,clust_kmed] = kmedoids((data_kmed),k_med,1);
    
    idx_kmed
    toc
    
    figure
    title_diffuse = 0;
    PlotKmeans(idx_k,C,(data_k),title_diffuse,Use_dim_red_kmean)
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%fuzz_c

if fuzzy_c
    disp('Timing Fuzzy C')
    tic
    if Use_Pca_C
        data_fc = pca(data,'NumComponents',2);
    else
        data_fc = data;
    end
    
    [idx_c,C,obj] =  fcm(data_fc,Nc,options);
    idx_c
    C
    figure
    PlotfuzzyC(idx_c,C,data_fc)
    toc
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%OPTICS
if Optics
disp('Timing Optics')
    tic
    if Use_dim_red_op
        data_OP = dim_red(data,'NumComponents');
    else
        data_OP = data;
    end
    
    [rd,cd,order] =  optics(data_OP', 8);
    order
    toc
%     figure
%     PlotClusterinResult(data_DB, idx_DB);
%     title(['DBSCAN Clustering (\epsilon = ' num2str(epsilon) ', MinPts = ' num2str(MinPts) ')']);
%     
% 
% 
end



%%%%%%%%%%Dimension Reduction Techniques
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Diffusion Mapping

if diffusionMaps
    disp('Timing Diffusion Maps')
    tic
    figure
    data_Dm = diffusion_maps(data',d,alpha,sigmaK);
    plot(data_Dm(:,1),data_Dm(:,2),'.')
    title(['Diffusion Maps Clustering (\sigmaK = ' num2str(sigmaK) ', alpha = ' num2str(alpha) ')']);
    toc
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%Diffusion Mapping with Kmeans

if Diffus_Kmean
    disp('Timing Diffusion Maping with K-means')
    tic
    title_diffuse = 1;
    data_mean_Dm = diffusion_maps(data_mean,sigmaK,alpha,d);
    [idx_diff,C,sumd,D] = kmeans(data_mean_Dm,k,'distance','sqeuclidean','Replicates',r);
    figure
    PlotKmeans(idx_diff,C,data_mean_Dm,title_diffuse)
    toc
end

if dim_reduction
    disp('Timing reduction Maps')
    tic
    figure
    data_Dm = dim_red(data',2);
    plot(data_Dm(:,1),data_Dm(:,2),'.')
    title(['PCA Reduction']);
    toc
end


if run_isomap
    disp('Timing reduction Maps')
    tic
    figure
    data_Dm = isomap(data',dim,neighboors);
    plot(data_Dm(:,1),data_Dm(:,2),'.')
    title(['PCA Reduction']);
    toc
end

if DB_Diffuse
    data_Dm = diffusion_maps(data',d,alpha,sigmaK);
    idx_DB_DM = DBSCAN(data_Dm,epsilon_DM,MinPts,d_type);
    PlotClusterinResult(data_Dm, idx_DB_DM);
end
