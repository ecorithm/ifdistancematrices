function PlotKmeans(idx, C,data_mean,title_diffuse,UsePCA)
if UsePCA == 0;
   data_mean=  pca((data_mean'),2);
end
    k=size(C,1);

    Colors=hsv(k);

    Legends = {};

    %data_mean = pca(data_mean','NumComponents',2);
 
hold on
    for i=1:k
         hold on;
        index = find(idx == i);
        plot(data_mean(index,1),data_mean(index,2),'o','Color',Colors(i,:))
        Legends{end+1} = ['Cluster #' num2str(i)];
        legend(Legends);
    

    end
    
%     
%     plot(C(:,1),C(:,2),'kx',...
%    'MarkerSize',15,'LineWidth',1)
if title_diffuse
 title(['K-means Clustering with Diffuse Mapping Reduction']);

else
 title(['K-means Clustering']);
end
 hold off
end