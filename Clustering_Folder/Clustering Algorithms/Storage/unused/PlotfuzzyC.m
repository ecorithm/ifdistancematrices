function PlotfuzzyC(idx, C,data_mean)

    k=size(idx,1);

    Colors=hsv(k);

    Legends = {};
    maxC = max(C);
    for i=1:k
         hold on;
        index = find(C(i,:) == maxC);
        plot(data_mean(index,1),data_mean(index,2),'o','Color',Colors(i,:))
        Legends{end+1} = ['Cluster #' num2str(i)];
        legend(Legends);
        title 'Fuzzy C Means';

    end
    for i=1:k
 
       plot(idx(i,1),idx(i,2),'x','MarkerSize',15,'LineWidth',1)
       axis square 
    end
end