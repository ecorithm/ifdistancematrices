%Calling function of the visualization functions
close all
clear all

colors={'r.' 'gx' 'b+' 'ys' 'm.' 'c.' 'k.' 'r*' 'g*' 'b*' 'y*' 'm*' 'c*' 'k*' };
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    data=csvread('data_test_spectral_Amp_week.csv',4,2);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
% %normalization of the data
data.X=data;
data=clust_normalize(data,'range');
% Clustering 


param.m=1;
param.c=4;
param.val=1;
param.vis=0;
param.e=1e-3;
param.ro=ones(1,param.c);

result=Kmeans(data,param);
%result=GKclust(data,param);
%result= GGclust(data,param);
%result = FCMclust(data,param);
%result = Kmedoid(data,param);
result=validity(result,data,param);
%Assignment for classification
[d1,d2]=max(result.data.f');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Principal Component Projection of the data and the cluster centers
break
param.q=2;
result = PCA(data,param,result); 
%visualization
figure(1)

clf

plot(result.proj.P(:,1),result.proj.P(:,2),'.');
hold on
plot(result.proj.vp(:,1),result.proj.vp(:,2),'r*');
break

%calculating realtion-indexes
result = SAMSTR1(data,result);
perf = [PROJEVAL1(result,param) result.proj.e];
%

disp('Press any key.')
pause   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%SAMMON mapping
% 
% proj.P=result.proj.P;   %Sammon uses the output of PCA for initializing
% param.alpha = 0.4;
% param.max=100;
% 
% figure(2)
% result = Sammon1(proj,data,result,param)
% %visualization
% clf
% % for i=1:max(C)
% %     index=find(C==i);
% %     err=(Cc(d2(index))~=i);
% %     eindex=find(err);
% %     misclass(i)=sum(err);
% %     plot(result.proj.P(index,1),result.proj.P(index,2),[colors{i}] )
% %     hold on
% %     plot(result.proj.P(index(eindex),1),result.proj.P(index(eindex),2),'o')
% %     hold on
% % end    
% %     xlabel('y_1')
% %     ylabel('y_2')
% %     title('Conventional Sammon mapping')
% % %
% plot(result.proj.P(:,1),result.proj.P(:,2),'.');
% hold on
% plot(result.proj.vp(:,1),result.proj.vp(:,2),'r*');    
% %calculating realtion-indexes
% result = SAMSTR1(data,result);
% perfs = [PROJEVAL1(result,param) result.proj.e];
% %
% disp('Press any key.')
% pause
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Modified fuzzy SAMMON mapping
  
proj.P=result.proj.P; %FuzSam uses the output of Sammon for initializing
param.alpha = 1;
param.max=200;

figure(3)
result=FuzSam(proj,result,param);
disp('Press any key.')
pause
hold on


 
plot(result.proj.P(:,1),result.proj.P(:,2),'.'); 
hold on
plot(result.proj.vp(:,1),result.proj.vp(:,2),'r*')
%calculating realtion-indexes
result = SAMSTR1(data,result);
perff = [PROJEVAL1(result,param) result.proj.e];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
perf
%perfs
perff






