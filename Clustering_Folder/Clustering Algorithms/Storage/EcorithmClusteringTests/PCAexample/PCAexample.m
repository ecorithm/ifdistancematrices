% generate PCA example
clear all
close all
%path(path,'..\..\FUZZCLUST')

data.X =csvread('data_test_spectral_Amp_week.csv',4,3);

%figure(1)
plot(data.X)

%break
%FCM clustering
param.c = 4;
param.m = 10;
result = FCMclust(data,param)

%break

%PCA projection
param.q = 10;
result = PCA(data,param,result);
figure(2)
plot(result.proj.P(:,1),result.proj.P(:,2),'.')
hold on
plot(result.proj.vp(:,1),result.proj.vp(:,2),'r*')
%perf = PROJEVAL(result,param);

%perf