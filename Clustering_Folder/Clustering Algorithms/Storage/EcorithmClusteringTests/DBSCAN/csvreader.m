function [data,headers,datesString]=csvreader(filename); %V1_1
fid = fopen(filename,'r');
data = {};
headers = {};
datesString = {};
count=1;
nbCol =1;

textline=fgetl(fid);
while ~isempty(textline)
    if ~ischar(textline),   break,   end
    [ headers{nbCol},r]=strtok(textline,',');
    if isempty(r),   break,   end
    textline=r;
    nbCol = nbCol +1;
end

data = csvread(filename,4,0);

textline=fgetl(fid);
nbdates = 1;
while size(textline,2)>2
    [datesString{nbdates},r]=strtok(textline,',');
    nbdates = nbdates +1;
    textline=fgetl(fid);
end

fclose(fid);