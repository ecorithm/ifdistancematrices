function [data_DB,data_Ang_DB,data_Amp_DB] = Dimension_Reduction(Use_dim_red_DB,dimension_reduction,data,data_Ang,data_Amp)
if Use_dim_red_DB;
    if strcmpi('pca',dimension_reduction)
        data_DB = pca(data');
        data_Ang_DB = pca(data_Ang');
        data_Amp_DB = pca(data_Amp');
    else strcmpi('diffusion_maps',dimension_reduction)
        data_DB = diffusion_maps(data',dim,alpha,sigma);
        data_Ang_DB = diffusion_maps(data_Ang',dim,alpha,sigma);
        data_Amp_DB = diffusion_maps(data_Amp',dim,alpha,sigma);
    end
    if Use_dim_red_DB_ang
        if strcmpi('pca',dimension_reduction)
            data_Ang_DB = pca(data_Ang');
        else strcmpi('diffusion_maps',dimension_reduction)
            data_Ang_DB = diffusion_maps(data_Ang',dim,alpha,sigma);
        end
    end
    
else
    data_DB = data';
    data_Amp_DB = data_Amp';
    data_Ang_DB = data_Ang' ;
end