function [idx_DB_new] = clust_ang(idx_DB,data_Ang_DB,epsilon_Ang,MinPts,d_type_Ang,dim,alpha,sigma,plot_ang_on)
if max(idx_DB) == 0
    error('Adjust Amplitude Epsislon value, All data is noise')
end
for i = 0:max(idx_DB); %%%%NOT SURE IF SHOULD START AT 0 or 1!!! NEED More testing
     int=  find(idx_DB == i);
     Data_Ang_new = data_Ang_DB(int,:);
    if i ==0
    idx_DB_new(int) = 0;   
    else
        idx_DB_ang = DBSCAN(Data_Ang_new,epsilon_Ang,MinPts,d_type_Ang);
        if plot_ang_on
            
            PlotClusterinResult(diffusion_maps(Data_Ang_new,dim,alpha,sigma),idx_DB_ang,['Angle Cluster' num2str(i)],0)
            figure
            title(num2str(i))
            %figure
            
        end
        if max(idx_DB_ang) <= 1
            
            idx_DB_new(int) = i;
            
        else max(idx_DB_ang) > 1; %If more than one cluster found in amplitude clusters
            int1 = int(find(idx_DB_ang<=1));
            int2 = int(find(idx_DB_ang>1));
            idx_DB_new(int1) = idx_DB(int1);
            idx_DB_new(int2) =  idx_DB_ang(find(idx_DB_ang>1)) +max(idx_DB)-1 ;
        end
    end
end