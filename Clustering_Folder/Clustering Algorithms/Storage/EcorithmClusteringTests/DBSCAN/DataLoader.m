    function [data_Amp,data_Ang,names] = DataLoader(filenameIn_Ang_day,filenameIn_Amp_day)

    %%Load Data 
    disp('Loading Data...')
    tic
    if Amp_day
        data_Amp=csvread(filenameIn_Amp_day,4,2);
        [~, names, ~] = csvreader2(filenameIn_Amp_day);
        names = names(3:end)';
    end
    % if Amp_week
        data_Amp = csvread(filenameIn_Amp_week,4,2);
        [~, names, ~] = csvreader2(filenameIn_Amp_week);
        names = names(3:end)';
    end
    if Ang_day
        data_Ang = csvread(filenameIn_Ang_day,4,2);
        [~, names, ~] = csvreader2(filenameIn_Ang_day);
        names = names(3:end)';
    end
    if Ang_week
        data_Ang = csvread(filenameIn_Ang_week,4,2);
        [~, names, ~] = csvreader2(filenameIn_Ang_week);
        names = names(3:end)';
    end
    toc
    end