function PlotKmeans(idx, C,data_mean)

    k=size(C,1);

    Colors=hsv(k);

    Legends = {};
    
    for i=1:k
         hold on;
        index = find(idx == i);
        plot(data_mean(index,1),data_mean(index,2),'o','Color',Colors(i,:))
        Legends{end+1} = ['Cluster #' num2str(i)];
        legend(Legends);
    

    end
      plot(C(:,1),C(:,2),'kx',...
   'MarkerSize',15,'LineWidth',1)
 title 'Kmeans'
 hold off
end