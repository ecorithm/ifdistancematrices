clc
clear
close all 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Parameters k = number of clusters ,number of centroid guess signify data set 

Amp_day = 0;
Amp_week = 0;
Ang_day = 0;
Ang_week =0; 
Amp_Ang_day = 1;
Amp_Ang_week =0; 

last = 32 ; % number of columns in data (i.e number of condtions) 

%%kmeans
K_means = 0;
k =4;
r = 50;

%%DBSCAN
dbscan = 1;
epsilon = .1;
MinPts = 4;

%Fuzzy_C_means
fuzzy_c = 1; 
Nc = 4;
options = [5 NaN NaN 0];

%GKclust

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%Load Data


if Amp_day 
    dataALL=csvread('data_test_spectral_Amp_day.csv',1,2,[1 2 2 last])';
end
if Amp_week 
    dataALL = csvread('data_test_spectral_Amp_week.csv',1,2)';
end
if Ang_day
    dataALL = csvread('data_test_spectral_Ang_day.csv',1,2,[1 2 2 last])';
end
if Ang_week
    dataALL = csvread('data_test_spectral_Ang_week.csv',1,2)';
end
if Amp_Ang_day 
    dataALL_ang = csvread('data_test_spectral_Ang_day.csv',1,2,[1 last 2 last])';
    dataALL_amp =csvread('data_test_spectral_Amp_day.csv',1,2,[1 last 2 last])';
end
if Amp_Ang_week 
    dataALL = csvread('data_test_spectral_Ang_day.csv',1,2)';
end




data_mean = dataALL_ang(:,1:2);
data_mean(:,3) = dataALL_amp(:,2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Kmeans
if K_means

[idx,C] = kmeans(data_mean,k,'distance','sqeuclidean','Replicates',r);
figure
 PlotKmeans(idx,C,data_mean)
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DBSCAN
if dbscan
    IDX = DBSCAN(data_mean,epsilon,MinPts);
    figure
    PlotClusterinResult(data_mean, IDX);
    title(['DBSCAN Clustering (\epsilon = ' num2str(epsilon) ', MinPts = ' num2str(MinPts) ')']);
    
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%fuzz_c

if fuzzy_c
    [idx,U] =  fcm(data_mean,Nc,options);
    figure
    PlotfuzzyC(idx,U,data_mean)
    for i = 1:Nc
    maxU = max(U);
    index{1,i} = find(U(i,:) == maxU)';
    end
    hold off
end
