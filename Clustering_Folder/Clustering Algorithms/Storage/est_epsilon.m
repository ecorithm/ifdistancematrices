%%epsilon estimation

function [epsil,IDX,D,SD] = est_epsilon(X,MinPts,d_type,hist_on,pcolor_on)
n=size(X,1);
count =0;
D=pdist2(X,X,d_type);
for i=1:n
    [IDX,D]=knnsearch(X(i,:),X);
     SD = SAM(X(i,:),X);
     %D
    %D(all(D==0,2),:)=[];
    %SD(all(D==0,2),:)=[];
    length(D);
    size(D);
    stor(i,:) = SD;
    epsil(i,:) = D;
    %count = count + 1
end
if hist_on
    
    hist(epsil)
    figure
end
if pcolor_on
    pcolor(epsil);
    shading flat
    figure
end
end