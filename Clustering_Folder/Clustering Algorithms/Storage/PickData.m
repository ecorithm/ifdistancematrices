function [data,data_plot] = PickData(data_Amp,data_Ang,scale_angle,scale)
if scale_angle
    data_Ang = data_Ang/scale;
end

data = data_Amp(:,1:end);
data_plot =[data_Amp(:,1:end);data_Ang(:,1:end)];

