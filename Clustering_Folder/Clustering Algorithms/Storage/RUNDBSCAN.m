%%Filmename_In_Amp/filenameIn_Ang is standard data, Filename_In_Comp is
%%added data. This Script takes the added data and compares it to the data
%%allready clustered, placing the new data in appropiate clusters..
function [] = RUNDBSCAN()
clc
clear
close all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Pick Data Day only 1 4 6,7,9,11 real data 10
%Real Data Dates 11/30/15 - 12/6/15 ESSB 275
Data_Set_Num = '12';
Data_Set_Num_Compare = '10';
Data_Set = ['Data/Set' Data_Set_Num '/' ];
Data_Set_Comp = ['Data/Set' Data_Set_Num_Compare '/' ];

%Algorithm Parameters
realdata = 1;
classify = 1;
start_comp = 1;
end_comp = 1000;
plot_ang_on = 0; %Turn clustering plotting of angles on Use when debugging epsilon for angle
MinPts = 20;
epsilon =.1;%.48 week ang amp % .09 for day %VAV_R35 Real Data 65 % VAV01 .20 % VAV01 Jan .23
epsilon_Ang = 68; %.28 Angle Scale 250, %69.9 for no scale %set7 .273 %VAV_R35 .25 % VAV01 .28 Dec % VAV01 .28 Jan



filenameIn_Amp_day = [Data_Set 'data_test_spectral_Amp_day_' Data_Set_Num '.csv'];
filenameIn_Amp_week = [Data_Set 'data_test_spectral_Amp_week_' Data_Set_Num '.csv'];
filenameIn_Ang_day = [Data_Set 'data_test_spectral_Ang_day_' Data_Set_Num '.csv'];
filenameIn_Ang_week = [Data_Set 'data_test_spectral_Ang_week_' Data_Set_Num '.csv'];

if realdata
    filenameIn_Ang_day_Comp = [Data_Set_Comp 'VAV_R01_Ang_Jan.csv'];
    filenameIn_Amp_day_Comp = [Data_Set_Comp 'VAV_R01_Amp_Jan.csv'];
else
    filenameIn_Amp_day_Comp = [Data_Set_Comp 'data_test_spectral_Amp_day_' Data_Set_Num_Compare '.csv'];
    filenameIn_Amp_week_Comp = [Data_Set_Comp 'data_test_spectral_Amp_week_' Data_Set_Num_Compare '.csv'];
    filenameIn_Ang_day_Comp = [Data_Set_Comp 'data_test_spectral_Ang_day_' Data_Set_Num_Compare '.csv'];
    filenameIn_Ang_week_Comp = [Data_Set_Comp 'data_test_spectral_Ang_week_' Data_Set_Num_Compare '.csv'];
end


%Go_DBSCAN(classify,plot_ang_on,epsilon,epsilon_Ang,MinPts,filenameIn_Ang_day,filenameIn_Amp_day,...
%  filenameIn_Amp_day_Comp,filenameIn_Ang_day_Comp,start_comp,end_comp)
Go_DBSCAN(classify,plot_ang_on,epsilon,epsilon_Ang,MinPts,filenameIn_Ang_day,filenameIn_Amp_day,...
    filenameIn_Amp_day_Comp,filenameIn_Ang_day_Comp,start_comp,end_comp)


end


    function []  = Go_DBSCAN(classify,plot_ang_on,epsilon,epsilon_Ang,MinPts,filenameIn_Ang_day,filenameIn_Amp_day,...
        filenameIn_Amp_day_Comp,filenameIn_Ang_day_Comp,start_comp,end_comp)

    Amp_day = 1;
    Amp_week = 0;
    Ang_day = 1;
    Ang_week =0;

    Use_Amp = 0; %Use Amplitude
    Use_Ang = 0; %Use Angle
    Use_Amp_Angle = 0; %Use Amplitude and Angle
    Use_Cos_Ang = 0; %Use Cosine of Angle
    Amp_Ang_sep = 1;
    scale_angle = 1;
    scale = 250;


    Est_Epsilon = 0;
    hist_on = 1;
    pcolor_on = 0;



    %%
    %%DBSCAN

    %%DBSCAN Setup

    %Dimension Reduction Parameters
    Use_dim_red_DB=0;
    Use_dim_red_DB_ang = 0;
    dimension_reduction = 'diffusion_maps'; %diffusion_maps %pca
    dim_red = str2func(dimension_reduction);
    dimplot=2;
    alpha = 1; %Diffusion_Maps Parameters
    sigma = 10;

    %Distance Metric
    d_type_Amp = 'minkowski';%'euclidean' 'seuclidean' 'mahalanobis' 'minkowski' 'correlation' 'cosine' 'cityblock' 'Jaccard'
    d_type_Ang = 'minkowski';%'euclidean' 'seuclidean' 'mahalanobis' 'minkowski' 'correlation' 'cosine' 'cityblock' 'Jaccard'


    %Load Data (Standard Vs. Comparison)
    [data_Amp,data_Ang, names] = DataLoader(filenameIn_Ang_day,filenameIn_Amp_day);
    [data_Amp_comp,data_Ang_comp,names_comp] = DataLoader(filenameIn_Ang_day_Comp,filenameIn_Amp_day_Comp);

    %Alters Data Based on Parameters
    [data,data_plot] =PickData(data_Amp,data_Ang,scale_angle,scale);
    [data_comp,data_plot_comp] = PickData(data_Amp_comp,data_Ang_comp,scale_angle,scale);

    %Dimension Reduction Function If Requested
    [data_DB,data_Ang_DB,data_Amp_DB] = Dimension_Reduction...
        (Use_dim_red_DB,dimension_reduction,data,data_Ang,data_Amp);
    [data_DB_comp,data_Ang_DB_comp,data_Amp_DB_comp] = Dimension_Reduction...
        (Use_dim_red_DB,dimension_reduction,data_comp,data_Ang_comp,data_Amp_comp);

    %Error Check for input Parameters
    if end_comp > size(data_Amp_DB_comp,1)
        disp('End point picked is greater than size of data, Setting End point to last point in data set')
        end_comp = size(data_Amp_DB_comp,1);
    end


    tic

    %Estimate Epsilon Tool
        if Est_Epsilon
            disp('Performing Epsilon Estimate...')
            est_epsilon(data',MinPts,d_type_Amp,hist_on,pcolor_on);
            toc
        end
    
        function [epsil,IDX,D,SD] = est_epsilon(X,MinPts,d_type,hist_on,pcolor_on)
        n=size(X,1);
        count =0;
        D=pdist2(X,X,d_type);
        for ind1=1:n
            [IDX,D]=knnsearch(X(i,:),X);
             SD = SAM(X(i,:),X);
             %D
            %D(all(D==0,2),:)=[];
            %SD(all(D==0,2),:)=[];
            length(D);
            size(D);
            stor(i,:) = SD;
            epsil(i,:) = D;
            %count = count + 1
        end
        if hist_on

            hist(epsil)
            figure
        end
        if pcolor_on
            pcolor(epsil);
            shading flat
            figure
        end
        end

    %%
    %DBSCAN Algorithm

    disp('Perfoming DBSCAN...')

    tic

    %%Comparison  Data

    function [IDX, isnoise]=DBSCAN(X,epsilon,MinPts,d_type,P)

    C=0;
    
    n=size(X,1);
    IDX=zeros(n,1);
    if strcmpi(d_type,'minkowski')
    D=pdist2(X,X,d_type);
    else
    D=pdist2(X,X,d_type);
    end
    
    visited=false(n,1);
    isnoise=false(n,1);
    
    for i=1:n
        if ~visited(i)
            visited(i)=true;
            
            Neighbors=RegionQuery(i);
            if numel(Neighbors)<MinPts
                % X(i,:) is NOISE
                isnoise(i)=true;
            else
                C=C+1;
                ExpandCluster(i,Neighbors,C);
            end
            
        end
    
    end
    
        function ExpandCluster(i,Neighbors,C)
            IDX(i)=C;

            k = 1;
            while true
                j = Neighbors(k);

                if ~visited(j)
                    visited(j)=true;
                    Neighbors2=RegionQuery(j);
                    if numel(Neighbors2)>=MinPts
                        Neighbors=[Neighbors Neighbors2];   %#ok
                    end
                end
                if IDX(j)==0
                    IDX(j)=C;
                end

                k = k + 1;
                if k > numel(Neighbors)
                    break;
                end
            end
        end

        function Neighbors=RegionQuery(i)
            Neighbors=find(D(i,:)<=epsilon);
        end

        end

    idx_DB = DBSCAN(data_DB,epsilon,MinPts,d_type_Amp);
    %testing_clust = fitgmdist(data_DB'*1000,1)
    idx_DB_new = clust_ang(idx_DB,data_Ang_DB,epsilon_Ang,MinPts,d_type_Ang,dimplot,alpha,sigma,plot_ang_on);
    
    %Adjust Data So Comparison Between Two Data Sets Can be made
    PlotClusterinResult(diffusion_maps(data_plot',dimplot,alpha,sigma),idx_DB_new,'Amplitude and Angle Class',0)


    %Now Classify New Data
    if classify
        count = 1;
        start_class = length(idx_DB_new)+1;

        %Class Amp
        plot_ang_on = 0;

        if size(data_Ang_DB_comp,2)~=size(data_Ang_DB,2); %%%% Problem Area
            disp('Data Set Sizes are Not equal, reducing dimension')
            dim = size(data_Ang_DB_comp,2);
            if strcmpi('diffusion_maps',dimension_reduction);
                data_Ang_DB= diffusion_maps(data_Ang_DB,dim,alpha,sigma);
                data_DB= diffusion_maps(data_DB,dim,alpha,sigma);
                %data_Ang_DB_comp = diffusion_maps(data_Ang_DB_comp,dim,alpha,sigma);
                %data_DB_comp = diffusion_maps(data_DB_comp,dim,alpha,sigma);
            else strcmpi('pca',dimension_reduction);
                data_Ang_DB= pca(data_Ang_DB,dim);
                data_DB= pca(data_DB,dim);
                % data_Ang_DB_comp= pca(data_Ang_DB_comp,dim);
                % data_DB= pca(data_DB_comp,dim);
            end
            data_DB_class = [data_DB;data_DB_comp(start_comp:end_comp,:)];
            data_DB_class = diffusion_maps(data_DB_class,dim,alpha,sigma);
        else
            data_DB_class = [data_DB;data_DB_comp(start_comp:end_comp,:)];
        end
        %%%%DEBUGGINGG
        idx_DB_class_int = DBSCAN(data_DB_class,epsilon,MinPts,d_type_Amp);


        %Class angle
        data_DB_class_ang = [data_Ang_DB;data_Ang_DB_comp(start_comp:end_comp,:)];
        idx_DB_class_ang = clust_ang(idx_DB_class_int,data_DB_class_ang,epsilon_Ang,MinPts,d_type_Ang,dimplot,alpha,sigma,plot_ang_on);


        idx_DB_class = idx_DB_class_ang(start_class:end);


        if end_comp > length(idx_DB_class)
            disp('End point chosen is greater than size of data, swithcing end point to last data point')
            end_comp = length(idx_DB_class);
        end

        %Recording Faults
        for j = start_comp:end_comp


            if idx_DB_class(count)==0
                name_class{count} = names_comp{j};
                fault_type{count} = 'Noise';
            else
                name_class{count} = names_comp{j};
                if idx_DB_class(count) > max(idx_DB_new)
                    fault_type{count} = ['New Fault' '  ' 'cluster' num2str(idx_DB_class(count))];
                else
                    fault_type{count} = names{find(idx_DB_class(count) == idx_DB_new,1)};
                end
            end
            count = count+1;
        end


    end





    %%Plot Classify Clusters
    hold on

    class_plot= diffusion_maps(data_plot_comp',dimplot,alpha,sigma);
    PlotClusterinResult(class_plot(start_comp:end_comp,:), idx_DB_class,'Amplitude and Angle Class',1)
    hold off


    %%
    %write File
    disp('Writing Cluster Results To .txt File...')
    tic

    fid = fopen('Clusters.csv', 'w+');
    for m = 1:length(idx_DB_new)
        fprintf(fid, '%6.0f %10s\n',idx_DB_new(m), names{m});
    end


    fileID = fopen('Clusters_Class.csv', 'w+');
    formatSpec = '%s %d %2.1f %s\n';


    nrows = length(idx_DB_class);
    formatspec = 'Cluster#: %2.0f PointName:    %35s     FaultType:     %12s\n';
    for i = 1:nrows

        fprintf(fileID,formatspec,idx_DB_class(i),name_class{i},fault_type{i});

    end
    fclose(fileID);
    toc
    title(['DBSCAN Clustering (\epsilon = ' num2str(epsilon) ', MinPts = ' num2str(MinPts) ')']);



end