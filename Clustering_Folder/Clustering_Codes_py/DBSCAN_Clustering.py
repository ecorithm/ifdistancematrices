# -*- coding: utf-8 -*-
"""
Created on Mon Mar 21 12:43:40 2016

@author: bradlittooy
"""
import pandas as pd
from sklearn.neighbors import DistanceMetric
from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn import decomposition
import heapq
import numpy as np
import matplotlib.pyplot as plt


#%%Estimate Average Cluster Size, #MinPts Cluster, #Epsilon Threshold
#These parameters really depend on reference singal data and should be known by user 

Epsilon_Threshold_Ang = 1.
Epsilon_Threshold_Amp = 1.5 #Adjust Size of Epsilon
MinPts = 90

#Turn on the plotting of Angle_Cluster Recommended set to False unless debugging
Plot_Each_Angle_Cluster = False

np.set_printoptions(threshold=np.nan)
plt.close("all")
#%%Loading Data
df_Amp_Ref = pd.read_csv('data_test_spectral_Amp_day_12.csv',index_col=0,low_memory=False)
df_Ang_Ref = pd.read_csv('data_test_spectral_Ang_day_12.csv',index_col=0,low_memory=False)
df_Amp_New = pd.read_csv('data_test_spectral_Amp_day_12.csv',index_col=0,low_memory=False)
df_Ang_New = pd.read_csv('data_test_spectral_Ang_day_12.csv',index_col=0,low_memory=False)

#%%
names = np.array(df_Amp_Ref.columns.values)[1:]
names_new = np.array(df_Amp_New.columns.values)[1:]
Point_Map_Ref={}
Point_Map_New={}
#Reference Data
data_Amp_Ref = np.array(df_Amp_Ref) 
data_Amp_Used_Ref = data_Amp_Ref[3:,1:].astype(np.float)
period = data_Amp_Ref[3:,0].astype(np.float)
data_Amp_trans_Ref = np.transpose(data_Amp_Used_Ref)
data_Ang_Ref = np.array(df_Ang_Ref) 
data_Ang_Used_Ref = data_Ang_Ref[3:,1:].astype(np.float)
data_Ang_trans_Ref = np.transpose(data_Ang_Used_Ref)

#New Data
data_Amp_New = np.array(df_Amp_New) 
data_Amp_Used_New = data_Amp_New[3:,1:].astype(np.float)
data_Amp_trans_New = np.transpose(data_Amp_Used_New)
data_Ang_New = np.array(df_Ang_New) 
data_Ang_Used_New = data_Ang_New[3:,1:].astype(np.float)
data_Ang_trans_New = np.transpose(data_Ang_Used_New)

map(Point_Map_Ref,names)
#map(Point_Map_Ref)
#%%
#Data for Plotting
pca = decomposition.PCA(n_components=2)
pca.fit(data_Amp_trans_Ref)
Plot_Data_Amp_ref = pca.transform(data_Amp_trans_Ref)
pca.fit(data_Ang_trans_Ref)
Plot_Data_Ang_ref = pca.transform(data_Ang_trans_Ref)

pca = decomposition.PCA(n_components=2)
pca.fit(data_Amp_trans_New)
Plot_Data_Amp_New = pca.transform(data_Amp_trans_New)
pca.fit(data_Ang_trans_New)
Plot_Data_Ang_New = pca.transform(data_Ang_trans_New)
#%%
data_Ang_trans_New_scale=data_Ang_trans_New
Amp_Angle = np.append([data_Amp_trans_New],[data_Ang_trans_New_scale],axis=2)[0]
Amp_Angle.shape
pca = decomposition.PCA(n_components=2)
pca.fit(Amp_Angle)
Plot_Amp_Angle = pca.transform(Amp_Angle)
plt.plot(Plot_Amp_Angle[:,0],Plot_Amp_Angle[:,1], 'o')
#plt.axis([-.1,.7,-.05,.2])
num_ref_signals = data_Ang_trans_Ref.shape[0]
#%%
#Plot_24_1_hr = data_Ang_trans_New

#%%
plot_test, = plt.plot(data_Amp_trans_New[:,143],data_Amp_trans_New[:,5],'o',markersize=15)

# Put a white cross over some of the data.
plt.legend([plot_test], ["Attr A"])

#%%
for i in range(0,len(data_Amp_Used_New)):
    plt.plot(period ,data_Amp_trans_New[i,:], 'o')
    plt.axis([-.05,24.5,-.05,.8])
    plt.xlabel('period (Hours)')
    plt.ylabel('Amplitude')



#%%Epsilon Estimate

distm = DistanceMetric.get_metric('euclidean')
pairwise_distance_Amp = distm.pairwise(data_Amp_trans_Ref)
pairwise_distance_Ang = distm.pairwise(data_Ang_trans_Ref)

#kde = KernelDensity(kernel='gaussian', bandwidth=0.2).fit(pairwise_distance)
#kde.score_samples(pairwise_distance)

def hist_epsil(pairwise_distance,bin_num,name = 'Histogram Plot'):
    hist, bins = np.histogram(pairwise_distance, bins=bin_num)
    width = 0.25 * (bins[1] - bins[0])
    center = (bins[:-1] + bins[1:]) / 2
    plt.figure()
    plt.bar(center, hist, align='center', width=width)
    plt.title(name)
    epsil = bins[1]
    return epsil

def grad_epsil(pairwise_distance,Average_Cluster_Size,Epsilon_Threshold,name='Distance Plot'):


    #test=abs(np.gradient(pairwise_distance[0,:]))
    #plt.plot(test)
    count =0
    i = 0
    epsil =[]
    while count < len(pairwise_distance):
        distance = pairwise_distance[count,:]
        count = count+round(len(pairwise_distance)/10,0)
        epsil.append(np.mean(heapq.nsmallest(Average_Cluster_Size,distance)))
        i +=1
    epsil = [x for x in epsil if x != 0] 
    
    epsil_guess= np.mean(epsil)*Epsilon_Threshold#Threshold for epsilon
    print('Estimated Epsilon Value: %f' % epsil_guess)
    plt.figure()
    plt.plot(distance)
    plt.title(name)
    return epsil_guess

Amp_epsil_1 = grad_epsil(pairwise_distance_Amp,MinPts,Epsilon_Threshold_Amp,
                            name ='Distance Plot Per Point (Amplitude)')
Amp_epsil = hist_epsil(pairwise_distance=pairwise_distance_Amp,
                       bin_num=4,name = 'Histogram Plot of Amplitude Distance')
Ang_epsil_1 = grad_epsil(pairwise_distance_Ang,MinPts,Epsilon_Threshold_Ang,
                         name ='Distance Plot Per Point (Angle)')
Ang_epsil = hist_epsil(pairwise_distance=pairwise_distance_Ang,bin_num=5,
                       name = 'Histogram Plot of Angle Distances')



#%%DBSCAN Amplitude Ref Signal
db_Amp = DBSCAN(eps=Amp_epsil_1, min_samples=MinPts).fit(data_Amp_trans_Ref)
core_samples_mask = np.zeros_like(db_Amp.labels_, dtype=bool)
core_samples_mask[db_Amp.core_sample_indices_] = True
labels = db_Amp.labels_

# Number of clusters in labels, ignoring noise if present.
n_clusters_Amp = len(set(labels)) - (1 if -1 in labels else 0)

print('Estimated number of clusters: %d' % n_clusters_Amp)
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(data_Amp_trans_Ref, labels))


for name, clust in zip(names,labels):
    print("Point: %s Cluster: %d" % (name,clust))   
    
unique_labels = set(labels)
colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels)))
plt.figure()

#Plot Amplitude Clusters
for k, col in zip(unique_labels, colors):
    if k == -1:
        # Black used for noise.
        col = 'k'
        
    class_member_mask = (labels == k)
       

plt.title('Estimated number of clusters (Amplitude): %d' % n_clusters_Amp)



#print(labels) data_Amp_trans_Ref

#%%DBSCAN Angle Ref Signal
for j in range(0,n_clusters_Amp):
    Angle_data= data_Ang_trans_Ref[np.where(labels==j)]
    Plot_Angle_Data_ref=Plot_Data_Ang_ref[np.where(labels==j)]
    db_Ang = DBSCAN(eps=Ang_epsil_1, min_samples=MinPts).fit(Angle_data)
    labels_Angle= db_Ang.labels_
    n_clusters_Ang = len(set(labels_Angle)) - (1 if -1 in labels_Angle else 0)
    core_samples_mask_angle = np.zeros_like(db_Ang.labels_, dtype=bool)
    core_samples_mask_angle[db_Ang.core_sample_indices_] = True
    #print('Estimated number of Sub-Clusters: %d' % n_clusters_Ang)
    labels_new = labels[:]
    if n_clusters_Ang>1 :
        labels_Angle[labels_Angle < 0] = 0
        labels_Angle[labels_Angle > 0] += n_clusters_Amp-1
        labels_Angle[labels_Angle == 0] = j 
        #print(labels_Angle)
        labels_new[np.where(labels==j)] = labels_Angle
    #%%Plot Each Angle Cluster Should be turned off most of time due to excessive
    #computation tme
    if Plot_Each_Angle_Cluster == True: 
        unique_labels_Angle = set(labels_Angle)
        colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels_Angle)))
        for k, col in zip(unique_labels_Angle,colors):
            if k == -1:
                #Black used for noise.
                col = 'k'
            class_member_mask_angle = (labels_Angle == k)
            xy_angle = Plot_Angle_Data_ref[class_member_mask_angle & core_samples_mask_angle]
            plt.plot(xy_angle[:, 0], xy_angle[:, 1], 'o', markerfacecolor=col,
                 markeredgecolor='k', markersize=10)
            xy_angle = Plot_Angle_Data_ref[class_member_mask_angle & ~core_samples_mask_angle]
            plt.plot(xy_angle[:, 0], xy_angle[:, 1], 'o', markerfacecolor=col,
                 markeredgecolor='k', markersize=10)
        plt.title('Cluster Number: %d' % j)
        plt.figure()           
#%%Plot New Cluster with Angle 

n_clusters_new = len(set(labels_new)) - (1 if -1 in labels_new else 0)
print('Estimated number of clusters after angle included: %d' % n_clusters_new)
unique_labels_new = set(labels_new)
core_samples_mask_new = np.zeros_like(labels_new, dtype=bool)
core_samples_mask_new[db_Amp.core_sample_indices_] = True
colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels_new)))
Legend=[]
cluster_name = []
for k, col in zip(unique_labels_new, colors):
    if k == -1:
        # Black used for noise.
        col = 'k'
    class_member_mask_new = (labels_new == k)
    xy_new = data_Amp_trans_Ref[class_member_mask_new & core_samples_mask_new]
    
    
    clsplt, = plt.plot(xy_new[:,100],xy_new[:,10], 'o', markerfacecolor=col,
    markeredgecolor='k', markersize=10)
    plt.axis([-.05,24.5,-.05,.8])
        
    xy_new = data_Amp_trans_Ref[class_member_mask_new & ~core_samples_mask_new]
   
       
    
    clsplt, =plt.plot(xy_new[:,100],xy_new[:,10], 'o', markerfacecolor=col,
    markeredgecolor='k', markersize=10,label='k')
    plt.axis([-.05,.25,-.05,.8])
    Legend.append(clsplt)
    cluster_name.append('Cluster Number: %d' %k)
plt.legend(Legend,cluster_name,numpoints=1)   
    
#print(labels)

plt.title('Estimated number of clusters (Angle + Amplitude): %d' % n_clusters_new)
plt.xlabel('Amplitude of Frequency of 1.43 Hr')
plt.ylabel('Amplitude of Frequency of 13 Hr')

#plt.figure()
        
#Classify
        
        #%%Amplitude
Data_Amp_Appended = np.append([data_Amp_trans_Ref], [data_Amp_trans_New],axis=1)[0]
db_Amp_New = DBSCAN(eps=Amp_epsil_1, min_samples=MinPts).fit(Data_Amp_Appended)
core_samples_mask_Amp_new = np.zeros_like(db_Amp_New.labels_, dtype=bool)
core_samples_mask_Amp_new[db_Amp_New.core_sample_indices_] = True
labels_Amp_New = db_Amp_New.labels_
unique_labels_Amp_New = set(labels_Amp_New)
colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels_Amp_New)))

pca = decomposition.PCA(n_components=2)
pca.fit(Data_Amp_Appended)
Plot_Data_Ampened = pca.transform(Data_Amp_Appended)
#print(labels_Amp_New)
#%%Plotting 

#print(labels)
n_clusters_Amp_new = len(set(labels_Amp_New)) - (1 if -1 in labels_Amp_New else 0)                 
for k, col in zip(unique_labels_Amp_New, colors):
    if k == -1:
        # Black used for noise.
        col = 'k'
    
    
    class_member_mask_Amp_New = (labels_Amp_New == k)
   
    Appended_Amp_Ref = Plot_Data_Ampened[class_member_mask_Amp_New[:num_ref_signals] &
                                            core_samples_mask_Amp_new[:num_ref_signals]]
                                       
    
    plt.plot(Appended_Amp_Ref[:, 0], Appended_Amp_Ref[:, 1], 'o', markerfacecolor=col,
             markeredgecolor='k', markersize=10)

    Appended_Amp_New = Plot_Data_Ampened[class_member_mask_Amp_New[num_ref_signals:]
                                        & core_samples_mask_Amp_new[num_ref_signals:]]        
    plt.plot(Appended_Amp_New[:, 0], Appended_Amp_New[:, 1], 's', markerfacecolor=col,
             markeredgecolor='k', markersize=5)
                 
    Appended_Amp_Ref = Plot_Data_Ampened[class_member_mask_Amp_New[:num_ref_signals]
                                        & ~core_samples_mask_Amp_new[:num_ref_signals]]
    plt.plot(Appended_Amp_Ref[:, 0], Appended_Amp_Ref[:, 1], 'o', markerfacecolor=col,
             markeredgecolor='k', markersize=10) 
    
    Appended_Amp_New = Plot_Data_Ampened[class_member_mask_Amp_New[num_ref_signals:]
                                        & ~core_samples_mask_Amp_new[num_ref_signals:]]        
    plt.plot(Appended_Amp_New[:, 0], Appended_Amp_New[:, 1], 's', markerfacecolor=col,
             markeredgecolor='k', markersize=5)
print('Estimated number of clusters (Classification): %d' % n_clusters_Amp)
plt.title('Estimated number of clusters Classification (Amplitude): %d' % n_clusters_Amp)
plt.figure()





#%%Angle               
Data_Ang_Appended = np.append([data_Ang_trans_Ref], [data_Ang_trans_New],axis=1)[0]
 
for m in range(0,n_clusters_Amp_new):
    Angle_data_append= Data_Ang_Appended[np.where(labels_Amp_New==m)]
    db_Ang_append = DBSCAN(eps=Ang_epsil_1, min_samples=MinPts).fit(Angle_data_append)
    labels_Angle_append= db_Ang_append.labels_
    n_clusters_Ang_append = len(set(labels_Angle_append)) - (1 if -1 in labels_Angle_append else 0)
    #print('Estimated number of Sub-Clusters: %d' % n_clusters_Ang_append)
    #print(m)
    #print(labels_Angle_append)
    labels_class = labels_Amp_New[:]
    if n_clusters_Ang>0 :
        labels_Angle_append[labels_Angle_append < 0] = 0
        labels_Angle_append[labels_Angle_append > 0] += n_clusters_Amp_new-1
        labels_Angle_append[labels_Angle_append == 0] = m 
        #print(labels_Angle)
        labels_class[np.where(labels_class==m)] = labels_Angle_append
        core_samples_mask_class = np.zeros_like(labels_Angle_append, dtype=bool)
        core_samples_mask_class[db_Ang_append.core_sample_indices_] = True   
n_clusters_class = len(set(labels_class)) - (1 if -1 in labels_class else 0)

#%%Amplitude + Angle Classification Plot
print('Estimated number of clusters after angle included (Classification): %d' % n_clusters_class)
unique_labels_class = set(labels_class)
colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels_class)))
for k, col in zip(unique_labels_class, colors):
    if k == -1:
        # Black used for noise.
        col = 'k'

    class_member_mask_Amp_New = (labels_class == k)
   
    Appended_Amp_Ref = Plot_Data_Amp_ref[class_member_mask_Amp_New[:num_ref_signals] &
                                            core_samples_mask_Amp_new[:num_ref_signals]]
                                       
    #print(Appended_Amp_Ref)
    plt.plot(Appended_Amp_Ref[:, 0], Appended_Amp_Ref[:, 1], 'o', markerfacecolor=col,
             markeredgecolor='k', markersize=10)
             
    Appended_Amp_New = Plot_Data_Amp_New[class_member_mask_Amp_New[num_ref_signals:]
                                        & core_samples_mask_Amp_new[num_ref_signals:]]        
    plt.plot(Appended_Amp_New[:, 0], Appended_Amp_New[:, 1], 's', markerfacecolor=col,
             markeredgecolor='k', markersize=5)

    Appended_Amp_Ref = Plot_Data_Amp_ref[class_member_mask_Amp_New[:num_ref_signals]
                                        & ~core_samples_mask_Amp_new[:num_ref_signals]]
    plt.plot(Appended_Amp_Ref[:, 0], Appended_Amp_Ref[:, 1], 'o', markerfacecolor=col,
             markeredgecolor='k', markersize=10) 
    
    Appended_Amp_New = Plot_Data_Amp_New[class_member_mask_Amp_New[num_ref_signals:]
                                        & ~core_samples_mask_Amp_new[num_ref_signals:]]        
    plt.plot(Appended_Amp_New[:, 0], Appended_Amp_New[:, 1], 's', markerfacecolor=col,
             markeredgecolor='k', markersize=5)
#print(labels)

plt.title('Estimated number of clusters Classification (Amplitude + Angle): %d' % n_clusters_new)
plt.show()
     
#print(labels)
#plt.title('Estimated number of clusters: %d' % n_clusters_new)
#plt.show()
        
#%%Plot Amplitude + Angle Classification
