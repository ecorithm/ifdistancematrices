# coding: utf-8

# In[1]:

import pandas as pd
from sklearn.neighbors import DistanceMetric
from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn import decomposition
import heapq
import numpy as np
import matplotlib.pyplot as plt


# In[2]:

#%%Estimate Average Cluster Size, #MinPts Cluster, #Epsilon Threshold
#These parameters really depend on reference singal data and should be known by user 

Epsilon_Threshold_Ang = 1.
Epsilon_Threshold_Amp = 1.5 #Adjust Size of Epsilon
MinPts = 90

#Turn on the plotting of Angle_Cluster Recommended set to False unless debugging
Plot_Each_Angle_Cluster = True

np.set_printoptions(threshold=np.nan)
plt.close("all")


# In[3]:

#%%Loading Data
df_Amp_Ref = pd.read_csv('data_test_spectral_Amp_day_12.csv',index_col=0,low_memory=False)
df_Ang_Ref = pd.read_csv('data_test_spectral_Ang_day_12.csv',index_col=0,low_memory=False)
df_Amp_New = pd.read_csv('data_test_spectral_Amp_day_12.csv',index_col=0,low_memory=False)
df_Ang_New = pd.read_csv('data_test_spectral_Ang_day_12.csv',index_col=0,low_memory=False)

#data_test_spectral_Amp_day_12

#Comparison_DATA_2day_Amp.csv
#ESSB276_AirFlow_Amp.csv

names = np.array(df_Amp_Ref.columns.values)[1:]
names_new = np.array(df_Amp_New.columns.values)[1:]
#Reference Data
data_Amp_Ref = np.array(df_Amp_Ref) 
data_Amp_Used_Ref = data_Amp_Ref[3:,1:]
data_Amp_trans_Ref = np.transpose(data_Amp_Used_Ref)
data_Ang_Ref = np.array(df_Ang_Ref) 
data_Ang_Used_Ref = data_Ang_Ref[3:,1:]
data_Ang_trans_Ref = np.transpose(data_Ang_Used_Ref)

#New Data
data_Amp_New = np.array(df_Amp_New) 
data_Amp_Used_New = data_Amp_New[3:,1:]
data_Amp_trans_New = np.transpose(data_Amp_Used_New)
data_Ang_New = np.array(df_Ang_New) 
data_Ang_Used_New = data_Ang_New[3:,1:]
data_Ang_trans_New = np.transpose(data_Ang_Used_New)

#Data for Plotting
pca = decomposition.PCA(n_components=2)
pca.fit(data_Amp_trans_Ref)
Plot_Data_Amp_ref = pca.transform(data_Amp_trans_Ref)
pca.fit(data_Ang_trans_Ref)
Plot_Data_Ang_ref = pca.transform(data_Ang_trans_Ref)

pca = decomposition.PCA(n_components=2)
pca.fit(data_Amp_trans_New)
Plot_Data_Amp_New = pca.transform(data_Amp_trans_New)
pca.fit(data_Ang_trans_New)
Plot_Data_Ang_New = pca.transform(data_Ang_trans_New)

num_ref_signals = data_Amp_trans_Ref.shape[0]


# In[4]:

#%%Epsilon Estimate

distm = DistanceMetric.get_metric('euclidean')
pairwise_distance_Amp = distm.pairwise(data_Amp_trans_Ref)
pairwise_distance_Ang = distm.pairwise(data_Ang_trans_Ref)

#kde = KernelDensity(kernel='gaussian', bandwidth=0.2).fit(pairwise_distance)
#kde.score_samples(pairwise_distance)

def hist_epsil(pairwise_distance,bin_num,name = 'Histogram Plot'):
    hist, bins = np.histogram(pairwise_distance, bins=bin_num)
    width = 0.25 * (bins[1] - bins[0])
    center = (bins[:-1] + bins[1:]) / 2
    plt.figure()
    plt.bar(center, hist, align='center', width=width)
    plt.title(name)
    epsil = bins[1]
    return epsil

def grad_epsil(pairwise_distance,Average_Cluster_Size,Epsilon_Threshold,name='Distance Plot'):


    #test=abs(np.gradient(pairwise_distance[0,:]))
    #plt.plot(test)
    count =0
    i = 0
    epsil =[]
    while count < len(pairwise_distance):
        distance = pairwise_distance[count,:]
        count = count+round(len(pairwise_distance)/10,0)
        epsil.append(np.mean(heapq.nsmallest(Average_Cluster_Size,distance)))
        i +=1
    epsil = [x for x in epsil if x != 0] 
    
    epsil_guess= np.mean(epsil)*Epsilon_Threshold#Threshold for epsilon
    print('Estimated Epsilon Value: %f' % epsil_guess)
    plt.figure()
    plt.plot(distance)
    plt.title(name)
    return epsil_guess

Amp_epsil_1 = grad_epsil(pairwise_distance_Amp,MinPts,Epsilon_Threshold_Amp,
                            name ='Distance Plot Per Point (Amplitude)')
Amp_epsil = hist_epsil(pairwise_distance=pairwise_distance_Amp,
                       bin_num=4,name = 'Histogram Plot of Amplitude Distance')
Ang_epsil_1 = grad_epsil(pairwise_distance_Ang,MinPts,Epsilon_Threshold_Ang,
                         name ='Distance Plot Per Point (Angle)')
Ang_epsil = hist_epsil(pairwise_distance=pairwise_distance_Ang,bin_num=5,
                       name = 'Histogram Plot of Angle Distances')




# In[12]:

#%%DBSCAN Amplitude Ref Signal
db_Amp = DBSCAN(eps=Amp_epsil_1, min_samples=MinPts).fit(data_Amp_trans_Ref)
core_samples_mask = np.zeros_like(db_Amp.labels_, dtype=bool)
core_samples_mask[db_Amp.core_sample_indices_] = True
labels = db_Amp.labels_

# Number of clusters in labels, ignoring noise if present.
n_clusters_Amp = len(set(labels)) - (1 if -1 in labels else 0)

print('Estimated number of clusters: %d' % n_clusters_Amp)
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(data_Amp_trans_Ref, labels))
 

unique_labels = set(labels)
colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels)))
plt.figure()

#Plot Amplitude Clusters
Legend = []
cluster_name = []
plt.figure(figsize=(12,10))
for k, col in zip(unique_labels, colors):
    if k == -1:
        # Black used for noise.
        col = 'k'  
    class_member_mask = (labels == k)
    label_name = names[class_member_mask] #choose the tenth point in the cluster for name.. Should be fixed Later 
    label_name_leg = label_name[10][0:-7]
    if len(label_name)>2*MinPts:
        label_name_leg = label_name[10][0:-7] + ' ' 'and' '  ' + names[class_member_mask][2*MinPts][0:-7] #Same as above
    elif k == -1:
        label_name_leg = 'noise'
    xy = Plot_Data_Amp_ref[class_member_mask]
    Cluster_Plot, = plt.plot(xy[:, 0], xy[:, 1],marker= 'o', markerfacecolor=col,
             markeredgecolor='k', markersize=10, linestyle="None" )
    cluster_name.append('Cluster Name: %s' %label_name_leg)
    Legend.append(Cluster_Plot)

plt.legend(Legend,cluster_name,loc='center left', bbox_to_anchor=(1, 0.5),numpoints=1,labelspacing=2,fontsize=16,markerscale=1.5)
    
    
plt.title('Estimated number of clusters (Amplitude): %d' % n_clusters_Amp,fontsize=20)
plt.figure()


#for name, clust in zip(names,labels):
    #print("Point Name: %s Cluster Number: %d" % (name,clust))


# In[16]:

#%%DBSCAN Angle Ref Signal
for j in range(0,n_clusters_Amp):
    names_angle=names[np.where(labels==j)]
    
    Angle_data= data_Ang_trans_Ref[np.where(labels==j)]
    Plot_Angle_Data_ref=Plot_Data_Ang_ref[np.where(labels==j)]
    db_Ang = DBSCAN(eps=Ang_epsil_1, min_samples=MinPts).fit(Angle_data)
    labels_Angle= db_Ang.labels_
    n_clusters_Ang = len(set(labels_Angle)) - (1 if -1 in labels_Angle else 0)
    core_samples_mask_angle = np.zeros_like(db_Ang.labels_, dtype=bool)
    core_samples_mask_angle[db_Ang.core_sample_indices_] = True
    #print('Estimated number of Sub-Clusters: %d' % n_clusters_Ang)
    labels_new = labels[:]
    if n_clusters_Ang>1 :
        labels_Angle[labels_Angle < 0] = 0
        labels_Angle[labels_Angle > 0] += n_clusters_Amp-1
        labels_Angle[labels_Angle == 0] = j 
        labels_new[np.where(labels==j)] = labels_Angle
        #%%Plot Each Angle Cluster Should be turned off most of time due to excessive
    #computation tme
    if Plot_Each_Angle_Cluster == True: 
        unique_labels_Angle = set(labels_Angle)
        colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels_Angle)))
        plt.figure(figsize=(12,10))
        Legend_angle = []
        cluster_name_angle = []
        for k, col in zip(unique_labels_Angle,colors):
            if k == -1:
                #Black used for noise.
                col = 'k'
            class_member_mask_angle = (labels_Angle == k)
            
            
            label_name_angle = names_angle[class_member_mask_angle]#choose the tenth point in the cluster for name.. Should be fixed Later 
            
            label_name_leg_angle = label_name_angle[0][0:-7]
            
           
            
            if len(label_name)>2*MinPts:
                label_name_leg_angle = label_name_angle[10][0:-7] + ' ' 'and' '  ' + names[class_member_mask_angle][-1][0:-7] #Same as above
            elif k == -1:
                label_name_leg_angle = 'noise'
            xy_angle = Plot_Angle_Data_ref[class_member_mask_angle]
            Cluster_Plot_angle, = plt.plot(xy_angle[:, 0], xy_angle[:, 1], 'o', markerfacecolor=col,
                 markeredgecolor='k', markersize=10)
            cluster_name_angle.append('Cluster Name: %s' %label_name_leg_angle)
            Legend_angle.append(Cluster_Plot_angle)
        
        plt.legend(Legend_angle,cluster_name_angle,loc='center left', bbox_to_anchor=(1, 0.5),numpoints=1,labelspacing=2,fontsize=16,markerscale=1.5)
        plt.title('Cluster Number: %d' % j, fontsize=20)
        plt.figure()
#Print Clusters and names


#for name, clust in zip(names,labels_new):
    #print("Point Name: %s Cluster Number: %d" % (name,clust)) 


# In[15]:

#%%Plot New Cluster with Angle 
n_clusters_new = len(set(labels_new)) - (1 if -1 in labels_new else 0)
print('Estimated number of clusters after angle included: %d' % n_clusters_new)
unique_labels_new = set(labels_new)
core_samples_mask_new = np.zeros_like(labels_new, dtype=bool)
core_samples_mask_new[db_Amp.core_sample_indices_] = True
colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels_new)))

Legend_new = []
cluster_name_new = []
plt.figure(figsize=(12,10))
for k, col in zip(unique_labels_new, colors):
    if k == -1:
        # Black used for noise.
        col = 'k'
    class_member_mask_new = (labels_new == k)
    label_name_new = names[class_member_mask_new]
    label_name_leg_new = label_name_new[10][0:-7]
    #Legend Only works for two in same cluster right now...
    if len(label_name_new)>2*MinPts:
        label_name_leg_new = label_name_new[10][0:-7] + ' ' 'and' '  ' + names[class_member_mask_new][2*MinPts][0:-7] #Same as above
    elif k == -1:
        label_name_leg_new = 'noise'
    xy_new = Plot_Data_Amp_ref[class_member_mask_new ]
    Cluster_Plot_new, = plt.plot(xy_new[:, 0], xy_new[:, 1], 'o', markerfacecolor=col,
             markeredgecolor='k', markersize=10,linestyle="None")
    cluster_name_new.append('Cluster Name: %s' %label_name_leg_new)
    Legend_new.append(Cluster_Plot_new)

   
plt.legend(Legend_new,cluster_name_new,loc='center left', bbox_to_anchor=(1, 0.5),numpoints=1,labelspacing=2,fontsize=16,markerscale=1.5)    
plt.title('Estimated number of clusters (Angle + Amplitude): %d' % n_clusters_new,fontsize=20)



# In[8]:

#Classify
        
        #%%Amplitude
Data_Amp_Appended = np.append([data_Amp_trans_Ref], [data_Amp_trans_New],axis=1)[0]
db_Amp_New = DBSCAN(eps=Amp_epsil_1, min_samples=MinPts).fit(Data_Amp_Appended)
core_samples_mask_Amp_new = np.zeros_like(db_Amp_New.labels_, dtype=bool)
core_samples_mask_Amp_new[db_Amp_New.core_sample_indices_] = True
labels_Amp_New = db_Amp_New.labels_
unique_labels_Amp_New = set(labels_Amp_New)
colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels_Amp_New)))
n_clusters_Amp_class = len(set(labels_Amp_New)) - (1 if -1 in labels_Amp_New else 0) 
pca = decomposition.PCA(n_components=2)
pca.fit(Data_Amp_Appended)
Plot_Data_Ampened = pca.transform(Data_Amp_Appended)
plt.figure()
print("Classified Points Amplitude Only")
print("Number of Clusters: %d" %n_clusters_Amp_class)
for name, clust, name_cl in zip(names_new,labels_Amp_New,names):
    print("Point Name: %s Cluster Number: %d Cluster Name: %s"  %(name[:-4],clust,name_cl[:-7]) ) 


# In[9]:

#%%Plotting 


#print(labels)
plt.figure(figsize=(12,8))
Legend_class = []
cluster_name_class = []
Legend_class_new=[]
cluster_name_class_new=[]
for k, col in zip(unique_labels_Amp_New, colors):
    if k == -1:
        # Black used for noise.
        col = 'k'
    
    
    class_member_mask_Amp_new = (labels_Amp_New == k)
    label_name_class = names[class_member_mask_Amp_new[0:num_ref_signals]] #choose the tenth point in the cluster for name.. Should be fixed Later 
    label_name_leg_class = label_name_class[10][0:-7]
    
    label_name_class_new = names_new[class_member_mask_Amp_new[num_ref_signals:]]
    label_name_leg_class_new = label_name_class_new[10][0:-7]
        
    if len(label_name_class)>2*MinPts:
        label_name_leg_class = label_name_class[10][0:-7] + ' ' 'and' '  ' + names[class_member_mask_Amp_new[0:num_ref_signals]][2*MinPts][0:-7] #Same as above
    elif k == -1:
        label_name_leg = 'noise'
    if len(label_name_class_new)>2*MinPts:
        label_name_leg_class_new = label_name_class_new[10][0:-7] + ' ' 'and' '  ' + names_new[class_member_mask_Amp_new[num_ref_signals:]][2*MinPts][0:-7] #Same as above
    elif k == -1:
        label_name_leg = 'noise'
    
    Appended_Amp_Ref = Plot_Data_Amp_ref[class_member_mask_Amp_new[:num_ref_signals]]
                                                                               
    plt.plot(Appended_Amp_Ref[:, 0], Appended_Amp_Ref[:, 1], 'o', markerfacecolor=col,
             markeredgecolor='k', markersize=15)
    
    Cluster_Plot_class, = plt.plot(xy[:, 0], xy[:, 1],marker= 'o', markerfacecolor=col,
             markeredgecolor='k', markersize=10, linestyle="None" )
    cluster_name_class.append(['Cluster Name: %s' %label_name_leg_class])
    Legend_class.append(Cluster_Plot_class) 
    
    Appended_Amp_New = Plot_Data_Amp_New[class_member_mask_Amp_new[num_ref_signals:]]
                                              
    plt.plot(Appended_Amp_New[:, 0], Appended_Amp_New[:, 1], 's', markerfacecolor=col,
             markeredgecolor='k', markersize=6)
    Cluster_Plot_class_new, =  plt.plot(Appended_Amp_New[:, 0], Appended_Amp_New[:, 1], 's', markerfacecolor=col,
             markeredgecolor='k', markersize=6)
    cluster_name_class_new.append(['Classification: %s' %label_name_leg_class_new])
    Legend_class_new.append(Cluster_Plot_class_new) 

    
first_legend = plt.legend(Legend_class,cluster_name_class,loc='right',numpoints=1,labelspacing=1.1,fontsize=8.5,markerscale=1,bbox_to_anchor=(-0.05, .75),
                          borderaxespad=0.,title='Training Points')
plt.gca().add_artist(first_legend)
plt.legend(Legend_class_new,cluster_name_class_new,loc='right',numpoints=1,labelspacing=1.1,fontsize=8.5,markerscale=1.1,bbox_to_anchor=(-.05, .25)
                          ,borderaxespad=0.,title='New Points')

print('Estimated number of clusters (Classification): %d' % n_clusters_Amp_class)
plt.title('Estimated number of clusters Classification (Amplitude): %d' % n_clusters_Amp_class,fontsize=20)
plt.figure()



# In[10]:

#%%Angle               
Data_Ang_Appended = np.append([data_Ang_trans_Ref], [data_Ang_trans_New],axis=1)[0]
 
for m in range(0,n_clusters_Amp_class):
    Angle_data_append= Data_Ang_Appended[np.where(labels_Amp_New==m)]
    db_Ang_append = DBSCAN(eps=Ang_epsil_1, min_samples=MinPts).fit(Angle_data_append)
    labels_Angle_append= db_Ang_append.labels_
    n_clusters_Ang_append = len(set(labels_Angle_append)) - (1 if -1 in labels_Angle_append else 0)
    #print('Estimated number of Sub-Clusters: %d' % n_clusters_Ang_append)
    #print(m)
    #print(labels_Angle_append)
    labels_class = labels_Amp_New[:]
    if n_clusters_Ang>0 :
        labels_Angle_append[labels_Angle_append < 0] = 0
        labels_Angle_append[labels_Angle_append > 0] += n_clusters_Amp_class-1
        labels_Angle_append[labels_Angle_append == 0] = m 
        #print(labels_Angle)
        labels_class[np.where(labels_class==m)] = labels_Angle_append
        core_samples_mask_class = np.zeros_like(labels_Angle_append, dtype=bool)
        core_samples_mask_class[db_Ang_append.core_sample_indices_] = True   
n_clusters_class = len(set(labels_class)) - (1 if -1 in labels_class else 0)
num_labels_new_class = labels_class[num_ref_signals:]
print('Estimated number of clusters after angle included (Classification): %d' % n_clusters_class)
#for name, clust, name_cl in zip(names_new,num_labels_new_class,names):
    #print("Classified Points")
    #print("Point Name: %s Cluster Number: %d Cluster Name: %s"  %(name[:-4],clust,name_cl[:-7]) ) 


# In[11]:

#%%Amplitude + Angle Classification Plot

plt.figure(figsize=(12,10))
Legend_class_angle = []
cluster_name_class_angle = []
Legend_class_new_angle=[]
cluster_name_class_new_angle=[]

unique_labels_class_ang = set(labels_class)
colors = plt.cm.Spectral(np.linspace(0, 1, len(unique_labels_class_ang)))
for k, col in zip(unique_labels_class_ang, colors):
    if k == -1:
        # Black used for noise.
        col = 'k'

    class_member_mask_new_angle = (labels_class == k)
    label_name_class_angle = names[class_member_mask_new_angle[0:num_ref_signals]] #choose the tenth point in the cluster for name.. Should be fixed Later 
    label_name_leg_class_angle = label_name_class_angle[10][0:-7]
    label_name_class_new_angle = names_new[class_member_mask_new_angle[num_ref_signals:]]
    label_name_leg_class_new_angle = label_name_class_new_angle[10][0:-7]
   
    
    if len(label_name_class_angle)>2*MinPts:
        label_name_leg_class_angle = label_name_class_angle[10][0:-7] + ' ' 'and' '  ' + names[class_member_mask_new_angle[0:num_ref_signals]][2*MinPts][0:-7] #Same as above
    elif k == -1:
        label_name_leg_angle = 'noise'
    if len(label_name_class_new_angle)>2*MinPts:
        label_name_leg_class_new_angle = label_name_class_new_angle[10][0:-7] + ' ' 'and' '  ' + names_new[class_member_mask_new_angle[num_ref_signals:]][2*MinPts][0:-7] #Same as above
    elif k == -1:
        label_name_leg_angle = 'noise'
    
    Appended_Amp_Ref = Plot_Data_Amp_ref[class_member_mask_new_angle[:num_ref_signals]]
                                        
                                       
    plt.plot(Appended_Amp_Ref[:, 0], Appended_Amp_Ref[:, 1], 'o', markerfacecolor=col,
             markeredgecolor='k', markersize=15)
    
    Cluster_Plot_class_angle, = plt.plot(xy[:, 0], xy[:, 1],marker= 'o', markerfacecolor=col,
             markeredgecolor='k', markersize=10, linestyle="None" )
    cluster_name_class_angle.append(['Cluster Name: %s' %label_name_leg_class_angle])
    Legend_class_angle.append(Cluster_Plot_class_angle) 
    
    Appended_Amp_New = Plot_Data_Amp_New[class_member_mask_new_angle[num_ref_signals:]]
                                                
    plt.plot(Appended_Amp_New[:, 0], Appended_Amp_New[:, 1], 's', markerfacecolor=col,
             markeredgecolor='k', markersize=6)
    
    Cluster_Plot_class_new_angle, =  plt.plot(Appended_Amp_New[:, 0], Appended_Amp_New[:, 1], 's', markerfacecolor=col,
             markeredgecolor='k', markersize=6)
    cluster_name_class_new_angle.append(['Classification: %s' %label_name_leg_class_new_angle])
    Legend_class_new_angle.append(Cluster_Plot_class_new_angle)

first_legend_angle = plt.legend(Legend_class_angle,cluster_name_class_angle,loc='right',numpoints=1,labelspacing=1.1,fontsize=10,markerscale=1,bbox_to_anchor=(-0.05, .75),
                          borderaxespad=0.,title='Training Points')
plt.gca().add_artist(first_legend_angle)
plt.legend(Legend_class_new_angle,cluster_name_class_new_angle,loc='right',numpoints=1,labelspacing=1.1,fontsize=10,markerscale=1.1,bbox_to_anchor=(-.05, .25)
                          ,borderaxespad=0.,title='New Points')



plt.title('Estimated number of clusters Classification (Amplitude + Angle): %d' % n_clusters_class)

        
#plt.title('Estimated number of clusters: %d' % n_clusters_class)
#plt.show()


# In[ ]:



