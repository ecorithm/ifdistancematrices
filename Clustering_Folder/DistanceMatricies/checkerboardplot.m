    %Checkerboard Plots
    clc
    clear
    close all
    %Types of Faults

    %'VAV_SAT_ABOVE_ST'    'Composite_HDF'    'R01'    'R02'

    %'R03'    'R04'    'R10'    'R11'    'R18'    'R19'    'R29'    'R30'

    %'R31'    'R32'    'R35'    'R36'    'R50'    'R51'    'R55'%

    % Pick Faults You want to Compare
    fault_1 = 'R11';
    fault_2 = 'R19';
    fault_3 = 'R03';
    real_data =1;

    %Load Data
    if real_data
    filename = 'merged.csv'; %data from feb 1 to feb 5
    [data,headers,~]=csvreader2(filename);
    f1 = find(strcmp(headers,fault_1))-1;
    f2 = find(strcmp(headers,fault_2))-1;
    f3 = find(strcmp(headers,fault_3))-1;
    else
        data = fakefault;
    end





    %Comapare Points and Plot Using checkerboard plot


        sim = zeros(size(data,2));
        tic
        for i = 1:size(data,2)
            for j = 1:size(data,2)
               
            sim(i,j) = dot(data(:,i),data(:,j))/sum(data(:,i))*100;
            
            
            if i ==j
                sim(i,j) = 0;
            end
         
            
            end
        end
        toc
    sim(isnan(sim))=0;
    %sim = sim/length(data)*100;
    pcolor(sim)
    h = colorbar;
    if size(data,2) > 50
    shading flat
    end
    ylabel(h,'Percent of Time Overlapping')
    xlabel('fault number')
    fault = 1:size(data,2);
    %csvwrite('Percentage_Overlap.csv',fault,0)
    csvwrite('Percentage_Overlap.csv',sim)


    %Distance Measurement Using Hamming distance metric

    %D=pdist2(data',data','Hamming');


    % %compare 2 faults
    % figure
    % D = pdist2(data(:,[f1 f2 ]),data(:,[f1 f2]),'Hamming');
    % pcolor(D)
    % shading flat
    % colorbar
    % title(['Comparing Faults' ' ' fault_1 ' with' fault_2 ])
    % 
    % 
    % 
    % %compare 3 faults
    % figure
    % D = pdist2(data(:,[f1 f2 f3]),data(:,[f1 f2 f3]),'Hamming');
    % pcolor(D)
    % shading flat
    % colorbar
    % title(['Comparing Faults' fault_1 ' with' fault_2 ' and' fault_3])
    % 


    % %Plotting All Faults together
    % figure
    % D1 = pdist2(data',data','Hamming');
    % %D1 = dot(data,data)
    % pcolor(D1)
    % title('All Faults')
    % colorbar
    % shading flat








%%optional Features



%  figure
% plot(data(:,f1))

%find(D==0)

% for i = 1:(size(data,2)-1) %%compare 2 faults
%     figure
%     D = pdist2(data(:,i),data(:,i+1),'Hamming');
%     pcolor(D)
%     shading flat
%     title(['Comparing Fault' headers{i} ' with Fault' headers{i+1}])
% end

%
% if f1 == 0 || f2 ==0 || f3 ==0
%     error('No Matching Fault for Fault_1 selection!')
% elseif f2 ==0
%     error('No Matching Fault for Fault_2 selection!')
%  else f3 == 0
%     error('No Matching Fault for Fault_3 selection!')
% end
