function [data,headers,datesString]=csvreader2(filename); %V1_1
fid = fopen(filename,'r');
data = {};
headers = {};
datesString = {};
count=1;
nbCol =1;

textline=fgetl(fid);
while ~isempty(textline)
    if ~ischar(textline),   break,   end
    [ headers{nbCol},r]=strtok(textline,',');
    if isempty(r),   break,   end
    textline=r;
    nbCol = nbCol +1;
end




format = ['%s',repmat('%f',1,size(headers,2)-1)];

data1 = textscan(fid, format,'Delimiter',',','headerlines',1,'EmptyValue',NaN);

data=[];
for k=1:size(headers,2)-1
    data(:,k) = data1{k+1};
end


datesString=data1{1};

fclose(fid);

if size(data,1)<size(datesString,2)
    datesString(end)=[];
end