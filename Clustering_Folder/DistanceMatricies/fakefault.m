%%Fake Faults

    function[data] =  fakefault(varargin)
        if length(varargin) ~=0
            num_faults=varargin{1};
            num_points=varargin{2};
        else
        num_faults = 1000;
        num_points = 24*60/5;
        end

    start = 8*60/5;
    finish = 18*60/5;
    for i = 1:num_faults
        val = repmat(de2bi(i),ceil(num_points/length(de2bi(i))),1);
        val = val(1:num_points);
        fault(:,i) = val(:);
    end
    if length(varargin) ~=0
    fault(1:start,:) = 0; fault(finish:end,:)=0;
    else
    data = fault;
    end

% plot(fault(:,3),'b')
% hold on
% plot(fault(:,5),'r')
% plot(fault(:,99),'g')
% plot(fault(:,150),'c')
