%%Fake Faults

function[data] =  fakefault(var)
num_faults = 1000;
num_points = 24*60/5; %nu
start = 8*60/5;
finish = 18*60/5;
for i = 1:num_faults
    val = repmat(de2bi(i),ceil(num_points/length(de2bi(i))),1);
    val = val(1:num_points);
    fault(:,i) = val(:);
end
fault(1:start,:) = 0; fault(finish:end,:)=0;
data = fault;
plot(fault(:,3),'b')
hold on
plot(fault(:,5),'r')
plot(fault(:,99),'g')
plot(fault(:,150),'c')
