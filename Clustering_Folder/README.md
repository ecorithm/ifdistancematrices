# Custering Related Codes

This is a repository for clustering related code. The Repository currently has four subfolders including Clustering, DistanceMatricies, Ecorithm ClusteringTests, and unused.

The Clustering folder contains code used for testing different types of data. It is currently set up for testing DBSCAN and contains a series of scripts to support that. The folder contains data which is used for testing as well.

Distancematricies contains code used for generating checkerboard plots. Currently one set of data looking at VAV01 is used and is under the data folder 'Merged.csv'

The other two folders aren't being used at the moment but contain code that could be used in the future. 
