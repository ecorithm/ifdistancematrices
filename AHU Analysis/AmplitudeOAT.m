clear all
close all
clc
%%Save File in Format Shown in ESB 276 or ESB 275 Folder. Data should be
%%saved as : Building Name/AHU_Name/AHU_name.csv or OAT.csv or VAV_name.csv
%%Example ESB-276/AHU_L1/AHU_L1.csv
%% Specify Building, AHU_Num, Temperature Limits and Percent of AHU in range
%%below and code will output plots corresponding to these limits. You can
%%run multiple scenearios at the same time. Code Will save plots to file as .jpg or
%%.fig under AHU Folder



plotnum = 0;
z = 1;


Building = 'ESSB 275_March';

AHU_Num = 'G1';
AHU_name = ['AHU_' AHU_Num];
AHU_Label = ['AHU' ' ' AHU_Num];
OAT_File = [Building '/' AHU_name '/' 'OAT.csv'];
AHU_File = [Building '/' AHU_name '/' 'AHU_' AHU_Num '.csv'];
VAV_File = [Building '/' AHU_name '/' 'VAV_' AHU_Num '.csv'];

Lower_Temp_Limit = [ 69 69.5 70  ];
Upper_Temp_Limit = [76];
Percentage = [ .75 ];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read OAT.csv, Find number of days %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[OAT, ~, dates] =  csvreader2(OAT_File);

dates = dates(2:end-1,1);

OAT_time = datenum(dates);

day_types = unique(floor(OAT_time));

num_days = length(day_types);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot OAT for 48 days overlaid %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% figure(1)
%
% for i = 1:num_days
%     OATdt = OAT(floor(OAT_time) == day_types(i));
%     plot(OATdt)
%     hold on
% end
%
% xlabel('12AM -> 12pm')
% ylabel('OAT')
% title('OAT 48 Days')
% axis([0 288 30 80])


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot FFT(OAT) for 48 days overlaid %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% figure(2)
%
% Fs = 1/300;
NFFT = 288;
%
%  f = Fs/2*linspace(0,1,NFFT/2+1);
%
%
for i = 1:num_days
    daily_OAT = OAT(floor(OAT_time) == day_types(i));
    temp = fft(daily_OAT)/NFFT;
    amplitude(i) = abs(temp(2));
    phases(i) = angle(temp(2));
    OAT_avg(i) = abs(temp(1));
    %plot(f(2:end),temp(2:NFFT/2+1))
    %hold on
end
%
% xlabel('x')
% ylabel('y')
% title('FFT(OAT) 48 Days')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Finding start/end time for AHU %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[AHU, ~, dates] = csvreader2(AHU_File);
%AHU = cell2mat(AHU_raw(2:end,2));
AHU(isnan(AHU)==1)=0;
AHU_time = dates(2:end-1,1);

AHU_time = datenum(AHU_time);

floor(AHU_time);

day_types = unique(floor(AHU_time));

num_days = length(day_types);



for i = 1:num_days
    startind = find(AHU(floor(AHU_time)==day_types(i)),1,'first');
    endind = find(AHU(floor(AHU_time)==day_types(i)),1,'last');
    day = AHU_time(floor(AHU_time)==day_types(i));
    if isempty(startind)==0 && isempty(endind)==0
        start_time(i) =  day(startind);
        end_time(i) = day(endind);
    else
        start_time(i) =  NaN;
        end_time(i) = NaN;
    end
end



%%%%%%%%%%%%%%%%%%%%%%%%
% Transient Start Time %
%%%%%%%%%%%%%%%%%%%%%%%%

[VAV, ~, dates] = csvreader2(VAV_File);

m = 0;
for tlow =  Lower_Temp_Limit
    for thigh = Upper_Temp_Limit
        for per = Percentage
            m = m +1;
            p_low = tlow;
            p_high = thigh;
            percent_B = per;
            
            
            strtlow = num2str(tlow);
            strthigh = num2str(thigh);
            strper = num2str(per);
            %name = [strtlow {' '} strthigh {' '} strper {' '} 'Transient Time'];
            for i = 1:num_days
                n = find(AHU_time==start_time(i));
                
                flag = 0;
                check = 0;
                if isempty(n)== 0
                    initial_temp(i) = mean(VAV(n,:));
                    while flag == 0
                        p = VAV(n,:);
                        
                        B = p<=p_high & p>=p_low;
                        percent_rooms = sum(B)/length(VAV(1,:));
                        
                        if percent_rooms >= percent_B
                            flag = 1;
                            transient(i) = AHU_time(n) - start_time(i);
                            if transient(i)*24 > 4
                                VAV_Long_Transient_end(i) = mean(VAV(n,:));
                                VAV_Long_Transient_start(i) = mean(VAV(n-check,:));
                                
                            end
                            
                        elseif floor(AHU_time(n)) - floor(start_time(i)) >= 1
                            flag = 1;
                            transient(i) = Inf;
                            %Day_e(z) = dates(n);
                            %z = z+1;
                        else
                            n = n + 1;
                            check = check + 1;
                        end
                        
                        
                    end
                    
                else
                    transient(i) = NaN;
                    initial_temp(i) = NaN;
                    
                end
                % if transient(i)*24 > 4
                %Day_e(z) = n;
                %internaltemp(z) = initial_temp(i);
                %Outside_temp(z) = OAT_avg(i);
                %       z = z +1;
                % end
                
            end
            transient = transient*24;
            
            
            %transient';
            [sum(transient==0) sum(isinf(transient))];
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Write Average OAT, Start Time, Phase, and Amplitude to .txt for GoSum %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            inputs_OAT = [OAT_avg' initial_temp'];
            inputs_Spectral = [OAT_avg' phases' amplitude' initial_temp'];
            outputs = [transient'];
            
            outputs(isnan(inputs_OAT(:,2))==1) = [];
            inputs_Spectral(isnan(inputs_OAT(:,2))==1,:) = [];
            inputs_OAT(isnan(inputs_OAT(:,2))==1,:) = [];
            inputs_Spectral(isinf(outputs)==1,:) = [];
            inputs_OAT(isinf(outputs)==1,:) = [];
            outputs(isinf(outputs)==1) = [];
            
            %outputs_initial_temp = inputs_OAT(:,3);
            tt = 1:length(transient);
            mean_out = ones(num_days,1)*mean(outputs);
            max_out = max(outputs);
            %  dlmwrite('inputs_OAT_69_76_85.txt' ,inputs_OAT,'delimiter', '\t')
            %  dlmwrite('inputs_spectal_69_76_85.txt' ,inputs_Spectral,'delimiter', '\t')
            %  dlmwrite('outputs_69_76_85.txt' ,outputs,'delimiter', '\t')
            tit = strcat(AHU_Label,' Tlow =',strtlow,', Thigh =', strthigh, ', Percent Satisfied = ', strper);
            figure
            stem(transient,'LineWidth',2)
            title(tit,'FontSize',13)
            xlabel('Day','FontSize',13)
            ylabel('Hours','FontSize',13)
            hold on
            plot(mean_out,'r')
            text(4,mean_out(1)*2+.05,['Average Transient Time:' ' ' num2str(round(mean_out(1),2)) ' ' 'hours'],'FontSize',13,'FontWeight','bold')
            text(4.7,mean_out(1)+.25,['Max Transient Time:' ' ' num2str(round(max_out,2)) ' ' 'hours'],'FontSize',13,'FontWeight','bold')
            hold off
            if ~exist(AHU_Label, 'dir')
                mkdir(AHU_Label);
            else
                if m ==1
                    delete([AHU_Label '/*.fig'])
                end
            end
            
            saveas(figure(m),[pwd '/' AHU_Label '/' tit '.fig'])
            saveas(figure(m),[pwd '/' AHU_Label '/' tit '.jpg'])
            hold off
            if plotnum == 50
                pause
                plotnum = 0;
            end
        end
    end
end
count = 0;
for k = 1:length(transient)
    if transient(k) > -1
        count = count + 1;
        temperature_start(count) = OAT_avg(k);
        initial_long(count) = initial_temp(k);
        transient_long(count) = transient(k);
    end
end
%close all
%
% figure
% subplot(3,1,1)
% plot(temperature_start)
% title('Outdoor Air Temp when transient time > 1 hr')
% xlabel('day')
% ylabel('Start Temperature')
%
%
% subplot(3,1,2)
% stem(transient_long,'LineWidth',2)
% title('Transient Time')
% xlabel('Day')
% ylabel('Hours')
%
%
% subplot(3,1,3)
% plot(initial_long)
% title('Initial Temp when transient time > 1 hr')
% xlabel('day')
% ylabel('Start Temperature')
%
% close all
% tlow = strcat('Tlow =',
% stem(transient,'LineWidth',2)
% title('Tlow = strtlow, Thigh = 76, Percent Satisifed = 85%')
% hold on
% plot(tt,4*ones(length(transient),1),'r')
% xlabel('Day')
% ylabel('Hours')
