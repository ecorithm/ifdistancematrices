# AHU Analysis
This script performs start time analysis on AHU's. Runs with AmplitudeOAT.m which
utilizes csvreader2.m for loading data. Script takes into account outdoor Air temp (Used for SVM models)
, VAV space temp to determine when space temp is comfortable, and AHU on off binary data to determine
when the AHU is on or off. These files should be saved as 'Building Name /AHU_Name/AHU_Name.csv  or /OAT.csv or /VAV_Name.csv '.
Script produces plots of transient time on a per day basis and saves the plots to the a folder specified as the name of the
AHU under .jpg and .fig. 
