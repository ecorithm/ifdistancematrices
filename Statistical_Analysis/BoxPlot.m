%%Currently Box Plot shows all devices per building
%%Spider plot shows per device across point types
%%Should Work for any building/point
clc
clear
close all

%Choose Month and Day, then choose building and device type, make sure
%building num and name are same ind in cell otherwise it will not work
Month = ['03'];
Day = ['01'];
Building_Nums = {['26'] ['27']};
Building = {['ESSB-275'] ['ESSB-276']};
Device = 'VAV';

%Place all point type names you want to examine, and Device names for
%spider plot. Choose hours for spectral data.

point_type = { ['SpaceTemp'] ['AirFlow'] ['AirflowSetPoint']}; %['SpaceTemp'] ['AirFlow'] ['AirflowSetPoint'] ['DamperPosition'] ['SupplyAirTemp']...
%['CoolingBias'] ['DamperCommand'] ['HeatingBias'] ['HeatingValveCommand'] ...
%['OccupiedCommand'] ['SupplyAirVelocityPressure'] ['WarmCoolAdjust']

Device_Names = {['VAV_301'] ['VAV_302']}; %  ['VAV_303']
hours = [24 12 8 6 4 2 1 .5];
hours_name = {['24 Hour'] ['12 Hour'] ['8 Hour'] ['6 Hour'] ['4 Hour'] ['2 Hour'] ['1 Hour'] ['.5 Hour']};

%Choose type of plot you want. Choose number of plots per page for box plot
boxplot_on = 1;
spider_plot_on = 1;
plots_page = 2;

%Runs for loops gathering data and performing analysis
ind = 0;
plot_count= 0;
for j = 1:length(Building)
    
    for i = 1:length(Device_Names)
        count = 0;
        
        for n = 1:length(point_type)
            try
                %Loading File... Might be a better way but works for now..
                %finds directory and then finds file name for csv read
                dirname = ['DATA/' Month '/' Day '/' 'building-' Building_Nums{j} '/' Building{j} '_' Device '_' point_type{n} '/*'];%Load Data
                file = dir(dirname);filename = {file.name};
                data_name =['DATA/' Month '/' Day '/' 'building-' Building_Nums{j} '/' Building{j} '_' Device '_' point_type{n} '/' filename{3}];
                data{n} = csvread(data_name,4,1);
                [~,names{n},~] = csvreader2(data_name);
                data_boxplot = data{n};
                for m = 1:length(hours)%Grab data from specified spectral hours
                    ind_hour(n,m) = find(round(data_boxplot(:,1),1)==hours(m),1);
                end
                
                %Box Plot
                data_new{n} = data_boxplot(ind_hour(n,:),2:end)';
                if boxplot_on && i ==1
                    if rem(n-1,plots_page) == 0 
                        figure
                        ind =0;
                    end
                    
                    ind = ind + 1;
%                     if ind>plots_page
%                         ind = 1;
%                     end
                    subplot(plots_page,1,ind)%Box plot data
                    boxplot(data_new{n},hours)
                    title([Building{j}  ' Spectral, ' Device ' ' point_type{n} ' Across All Devices'] )
                    ylabel('Amplitude','FontSize',14)
                    xlabel('Spectral Hour','FontSize',14)
                    hold on
                end
                
                %Spider Plot, currently normalized
                if spider_plot_on
                    count = count+1;
                    dev_ind(i) = strmatch(Device_Names{i},names{n})-1;
                    data_device(:,count) = ((data_boxplot(ind_hour(n,:),dev_ind(i))-min(data_boxplot(ind_hour(n,:),dev_ind(i))))...
                        /(max(data_boxplot(ind_hour(n,:),dev_ind(i))) - min(data_boxplot(ind_hour(n,:),dev_ind(i)))))';
                    legend_spider{count} = point_type{n};
                end
                
                
            catch
                display('Filename Does Not Exist, Skiping iteration')
                display([dirname])
            end
            
        end
        try
        if spider_plot_on
            data_device_num{i} = data_device;
            spider(data_device_num{i},[Building{j} ' ' Device_Names{i}],[0 1],hours_name,legend_spider);
        end
        catch
        end
    end
end





